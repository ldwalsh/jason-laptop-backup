﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="KGSEquipmentVariableAdministration.aspx.cs" Inherits="CW.Website.KGSEquipmentVariableAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                <asp:Literal ID="litScripts" runat="server" />
                    	                  	
            	<h1>KGS Equipment Variables Administration</h1>                        
                <div class="richText">The kgs equipment variables administration area is used to view, add, and edit equipment variables.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                       
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
               </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Equipment Variables"></telerik:RadTab>
                            <telerik:RadTab Text="Add Equipment Variable"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Equipment Variables</h2> 
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p>  
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>          
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridEquipmentVariables"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="EVID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridEquipmentVariables_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridEquipmentVariables_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridEquipmentVariables_Sorting"  
                                             OnSelectedIndexChanged="gridEquipmentVariables_OnSelectedIndexChanged"                                                                                                             
                                             OnRowEditing="gridEquipmentVariables_Editing"                                                                                                                                                                                   
                                             OnRowDeleting="gridEquipmentVariables_Deleting"
                                             OnDataBound="gridEquipmentVariables_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:CommandField ShowSelectButton="true" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentVariableName" HeaderText="Equipment Variable Name">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("EquipmentVariableName"),30) %></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="EquipmentVariableDisplayName" HeaderText="Equipment Variable Display Name">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("EquipmentVariableDisplayName"),30) %></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="IP Eng Units">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("IPEngUnits"),12) %></ItemTemplate>                          
                                                </asp:TemplateField>
                                                 <asp:TemplateField ItemStyle-Wrap="true" HeaderText="SI Eng Units">  
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("SIEngUnits"),12) %></ItemTemplate>                          
                                                </asp:TemplateField>   
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="User Editable">                                                      
                                                    <ItemTemplate><%# Eval("UserEditable") %></ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="IP Default Val">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("IPDefaultValue"),14) %></ItemTemplate>
                                                </asp:TemplateField>                                                    
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="SI Default Val">                                                      
                                                    <ItemTemplate><%# StringHelper.TrimText(Eval("SIDefaultValue"),14) %></ItemTemplate>
                                                </asp:TemplateField>
                                                 <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton Runat="server" CausesValidation="false"
                                                               OnClientClick="return confirm('Are you sure you wish to delete this equipment variable permanently?');"
                                                               CommandName="Delete">Delete</asp:LinkButton>  
                                                    </ItemTemplate>
                                                </asp:TemplateField>                                                                                                                                                                          
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br /><br /> 
                                    <div>                                      
                                    
                                    <!--SELECT EQUIPMENT VARIABLE DETAILS VIEW -->
                                    <asp:DetailsView ID="dtvEquipmentVariable" runat="server" AutoGenerateRows="false" BorderWidth="1" BorderColor="White" BorderStyle="Solid" >
                                        <Fields>
                                            <asp:TemplateField  
                                            ControlStyle-BorderWidth="0" ControlStyle-BorderColor="White" ControlStyle-BorderStyle="none" 
                                            ItemStyle-BorderWidth="0" 
                                            ItemStyle-BorderColor="White" 
                                            ItemStyle-BorderStyle="none"
                                            ItemStyle-Width="100%"
                                            HeaderStyle-Width="1" 
                                            HeaderStyle-BorderWidth="0" 
                                            HeaderStyle-BorderColor="White" 
                                            HeaderStyle-BorderStyle="none"
                                            >
                                                <ItemTemplate>
                                                    <div>
                                                        <h2>Equipment Variable Details</h2>                                                                                                                                            
                                                        <ul class="detailsList">
                                                            <%# "<li><strong>Equip. Variable Name: </strong>" + Eval("EquipmentVariableName") + "</li>"%> 
                                                            <%# "<li><strong>Equip. Var. Display Name: </strong>" + Eval("EquipmentVariableDisplayName") + "</li>"%> 
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("EquipmentVariableDescription"))) ? "" : "<li><strong>Description: </strong>" + Eval("EquipmentVariableDescription") + "</li>"%>                                                           
                                                            <%# "<li><strong>IP Engineering Units: </strong>" + Eval("IPEngUnits") + "</li>"%>
                                                            <%# "<li><strong>SI Engineering Units: </strong>" + Eval("SIEngUnits") + "</li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("UserEditable"))) ? "" : "<li><strong>User Editable: </strong>" + Eval("UserEditable") + "</li>"%>
                                                            <!--Default value can be an empty string-->
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("IPDefaultValue"))) ? "" : "<li><strong>IP Default Value: </strong><div class='divContentPreWrap'>" + Eval("IPDefaultValue") + "</div></li>"%>
                                                            <%# String.IsNullOrEmpty(Convert.ToString(Eval("SIDefaultValue"))) ? "" : "<li><strong>SI Default Value: </strong><div class='divContentPreWrap'>" + Eval("SIDefaultValue") + "</div></li>"%>
                                                         </ul>
                                                    </div>
                                                </ItemTemplate>                                             
                                            </asp:TemplateField>
                                        </Fields>
                                    </asp:DetailsView>   
                                    </div>
                                    
                                    <!--EDIT EQUIPMENT PANEL -->                              
                                    <asp:Panel ID="pnlEditEquipmentVariable" runat="server" Visible="false" DefaultButton="btnUpdateEquipmentVariable">                                                                                             
                                        <div>
                                            <h2>Edit Equipment Variable</h2>
                                        </div>  
                                        <div>    
                                            <a id="lnkSetFocusEdit" runat="server"></a>                                                 
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Equipment Variable Name:</label>
                                                <asp:TextBox ID="txtEditEquipmentVariableName" MaxLength="50" runat="server"></asp:TextBox>
                                                <p>(Warning: Editing a equipment variable name may cause multiple analyses to stop working.)</p>
                                                <asp:HiddenField ID="hdnEquipmentVariableName" runat="server" Visible="false" />
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Equipment Variable Display Name:</label>
                                                <asp:TextBox ID="txtEditEquipmentVariableDisplayName" MaxLength="50" runat="server"></asp:TextBox>                                    
                                                <asp:HiddenField ID="hdnEquipmentVariableDisplayName" runat="server" Visible="false" />
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 500, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>  
                                            <div class="divForm">   
                                                <label class="label">*IP Engineering Units:</label>    
                                                <asp:DropDownList ID="ddlEditIPEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">   
                                                <label class="label">*SI Engineering Units:</label>    
                                                <asp:DropDownList ID="ddlEditSIEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">                             
                                                </asp:DropDownList> 
                                            </div>
                                            <div class="divForm">
                                                <label class="label">*User Editable:</label> 
                                                <asp:CheckBox CssClass="checkbox" ID="chkEditUserEditable" runat="server" />
                                            </div>
                                            <div class="divForm">
                                                <label class="label">IP Default Value:</label>
                                                <asp:TextBox ID="txtEditIPDefaultValue" MaxLength="25" runat="server" TextMode="SingleLine" Rows="1" Columns="1" CssClass="textbox"></asp:TextBox>&nbsp;<span id="spEditIPInfo" runat="server"></span>
                                                <div id="valFailureEditIPDefaultValue" runat="server" style="color:red; display:none"></div>
                                            </div>
                                            <div class="divForm">
                                                <label class="label">SI Default Value:</label>
                                                <asp:TextBox ID="txtEditSIDefaultValue" MaxLength="25" runat="server" TextMode="SingleLine" Rows="1" Columns="1" CssClass="textbox"></asp:TextBox>&nbsp;<span id="spEditSIInfo" runat="server"></span>
                                                <div id="valFailureEditSIDefaultValue" runat="server" style="color:red; display:none"></div>
                                            </div>                                                                                                  
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateEquipmentVariable" runat="server" Text="Update Variable"  OnClick="updateEquipmentVariableButton_Click" ValidationGroup="EditEquipmentVariable"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editEquipmentVariableNameRequiredValidator" runat="server"
                                        ErrorMessage="Equipment Variable Name is a required field."
                                        ControlToValidate="txtEditEquipmentVariableName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditEquipmentVariable">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editEquipmentVariableNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editEquipmentVariableNameRequiredValidatorExtender"
                                        TargetControlID="editEquipmentVariableNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="editEquipmentVariableDisplayNameRequiredValidator" runat="server"
                                        ErrorMessage="Equipment Variable Display Name is a required field."
                                        ControlToValidate="txtEditEquipmentVariableDisplayName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditEquipmentVariable">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editEquipmentVariableDisplayNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editEquipmentVariableDisplayNameRequiredValidatorExtender"
                                        TargetControlID="editEquipmentVariableDisplayNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="editIPEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="IP Engineering Units is a required field." 
                                        ControlToValidate="ddlEditIPEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditEquipmentVariable">
                                        </asp:RequiredFieldValidator>
                                      <ajaxToolkit:ValidatorCalloutExtender ID="editIPEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="editIPEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="editIPEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="editSIEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="SI Engineering Units is a required field." 
                                        ControlToValidate="ddlEditSIEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="EditEquipmentVariable">
                                        </asp:RequiredFieldValidator>
                                      <ajaxToolkit:ValidatorCalloutExtender ID="editSIEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="editSIEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="editSIEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                    </asp:Panel>
                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">  
                                <asp:Panel ID="pnlAddEquipmentVariable" runat="server" DefaultButton="btnAddEquipmentVariable"> 
                                    <h2>Add Equipment Variable</h2>
                                    <div>          
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                          
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Equipment Variable Name:</label>
                                            <asp:TextBox ID="txtAddEquipmentVariableName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">*Equipment Variable Display Name:</label>
                                            <asp:TextBox ID="txtAddEquipmentVariableDisplayName" CssClass="textbox" Columns="50" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div>
                                        <div class="divForm">
                                             <label class="label">Description:</label>
                                             <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 500, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                             <div id="divAddDescriptionCharInfo"></div>
                                        </div>  
                                        <div class="divForm">   
                                            <label class="label">*IP Engineering Units:</label>    
                                            <asp:DropDownList ID="ddlAddIPEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div>
                                        <div class="divForm">   
                                            <label class="label">*SI Engineering Units:</label>    
                                            <asp:DropDownList ID="ddlAddSIEngUnits" CssClass="dropdown" AppendDataBoundItems="true" runat="server">
                                                 <asp:ListItem Value="-1">Select one...</asp:ListItem>                                 
                                            </asp:DropDownList> 
                                        </div>   
                                        <div class="divForm">
                                            <label class="label">IP Default Value:</label>
                                            <asp:TextBox ID="txtAddIPDefaultValue" MaxLength="25" runat="server" TextMode="SingleLine" Rows="1" Columns="1" CssClass="textbox"></asp:TextBox>&nbsp;<span id="spAddIPInfo" runat="server"></span>
                                            <div id="valFailureAddIPDefaultValue" runat="server" style="color:red; display:none"></div>
                                        </div>
                                        <div class="divForm">
                                            <label class="label">SI Default Value:</label>
                                            <asp:TextBox ID="txtAddSIDefaultValue" MaxLength="25" runat="server" TextMode="SingleLine" Rows="1" Columns="1" CssClass="textbox"></asp:TextBox>&nbsp;<span id="spAddSIInfo" runat="server"></span>
                                            <div id="valFailureAddSIDefaultValue" runat="server" style="color:red; display:none"></div>
                                        </div>  
                                        <div class="divForm">
                                            <label class="label">*User Editable:</label> 
                                            <asp:CheckBox CssClass="checkbox" ID="chkAddUserEditable" runat="server" />
                                        </div>                                                                                                 
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddEquipmentVariable" runat="server" Text="Add Variable"  OnClick="addEquipmentVariableButton_Click" ValidationGroup="AddEquipmentVariable"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addEquipmentVariableNameRequiredValidator" runat="server"
                                        ErrorMessage="Equipment Variable Name is a required field."
                                        ControlToValidate="txtAddEquipmentVariableName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddEquipmentVariable">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentVariableNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addEquipmentVariableNameRequiredValidatorExtender"
                                        TargetControlID="addEquipmentVariableNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addEquipmentVariableDisplayNameRequiredValidator" runat="server"
                                        ErrorMessage="Equipment Variable Display Name is a required field."
                                        ControlToValidate="txtAddEquipmentVariableDisplayName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddEquipmentVariable">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addEquipmentVariableDisplayNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addEquipmentVariableDisplayNameRequiredValidatorExtender"
                                        TargetControlID="addEquipmentVariableDisplayNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addIPEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="IP Engineering Units is a required field." 
                                        ControlToValidate="ddlAddIPEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddEquipmentVariable">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addIPEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="addIPEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="addIPEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>
                                    <asp:RequiredFieldValidator ID="addSIEngUnitsRequiredValidator" runat="server"
                                        ErrorMessage="SI Engineering Units is a required field." 
                                        ControlToValidate="ddlAddSIEngUnits"  
                                        SetFocusOnError="true" 
                                        Display="None" 
                                        InitialValue="-1"
                                        ValidationGroup="AddEquipmentVariable">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addSIEngUnitsRequiredValidatorExtender" runat="server"
                                        BehaviorID="addSIEngUnitsRequiredValidatorExtender" 
                                        TargetControlID="addSIEngUnitsRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>  
                                </asp:Panel>                                                                                                                                                                
                            </telerik:RadPageView>                        
                        </telerik:RadMultiPage>
                   </div>                                                                 
              
</asp:Content>


                    
                  
