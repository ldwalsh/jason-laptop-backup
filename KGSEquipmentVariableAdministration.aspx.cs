﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.Helpers;
using CW.Data.Models.Equipment;
using CW.Data.Models.EquipmentVariable;
using CW.Utility.Extension;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSEquipmentVariableAdministration : SitePage
    {
        #region Properties
            private EquipmentVariable mEquipmentVariable;
            const string addEquipmentVariableSuccess = " equipment variable addition was successful.";
            const string addEquipmentVariableFailed = "Adding equipment variable failed. Please contact an administrator.";
            const string addEquipmentVariableNameExists = "Cannot add equipment variable because the name already exists.";
            const string addEquipmentVariableDisplayNameExists = "Cannot add equipment variable because the display name already exists."; 
            const string updateSuccessful = "Equipment variable update was successful.";
            const string updateFailed = "Equipment variable update failed. Please contact an administrator.";
            const string updateFailedCorrectErrors = "Equipment variable update failed. Please correct errors below.";
            const string updateEquipmentVariableNameExists = "Cannot update equipment variable name because the name already exists.";
            const string updateEquipmentVariableDisplayNameExists = "Cannot update equipment variable display name because the display name already exists.";     
            const string deleteSuccessful = "Equipment variable deletion was successful.";
            const string deleteFailed = "Equipment variable deletion failed. Please contact an administrator.";            
            const string associatedToEquipment = "Cannot delete equipment variable becuase an equipment requires it.";
            const string associatedToAnalysis = "Cannot delete equipment variable becuase an analysis requires it.";
            const string associatedToEquipmentClass = "Cannot delete equipment variable becuase its associated to one or more equipment classes. Please disassociate from classes first.";
            const string initialSortDirection = "ASC";
            const string initialSortExpression = "EquipmentVariableDisplayName";
            const string equipmentvariableExists = "Equipment Variable with provided EVID already exists.";
            const string equipmentVariableStringOrBool = "Both IP and SI Engineering Units must be the same if one is a string or a boolean.";
            protected string selectedValue;   
            
            //gets and sets the gridview viewstate sort direction for the dataview
            private string GridViewSortDirection
            {
                get { return ViewState["SortDirection"] as string ?? string.Empty; }
                set { ViewState["SortDirection"] = value; }
            }

            //gets and sets the gridview viewstate sort expression for the dataview
            private string GridViewSortExpression
            {
                get { return ViewState["SortExpression"] as string ?? string.Empty; }
                set { ViewState["SortExpression"] = value; }
            }

        #endregion

        #region Page Events

            protected void Page_Init(Object sender, EventArgs e)
            {
                litScripts.Text = CW.Website._masters.ReferenceSetter.Get("buildingequipmentvariables");
            }
            protected void Page_Load(object sender, EventArgs e)
            {
                //logged in security check in master page.
                //secondary security check specific to this page.
                //Check kgs super admin or higher.
                if (!siteUser.IsKGSSuperAdminOrHigher)
                    Response.Redirect("/Home.aspx");

                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;

                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    BindEquipmentVariables();

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindAllEngUnitsDropdowns();

                    sessionState["Search"] = String.Empty;
                }
            }

        #endregion

        #region Set Fields and Data

            protected void SetEquipmentVariableIntoEditForm()
            {
                //ID
                hdnEditID.Value = Convert.ToString(mEquipmentVariable.EVID);
                //Hidden Equipment Vairable Name
                hdnEquipmentVariableName.Value = mEquipmentVariable.EquipmentVariableName;
                //Equipment Variable Name
                txtEditEquipmentVariableName.Text = mEquipmentVariable.EquipmentVariableName;
                //Hidden Equipment Vairable Display Name
                hdnEquipmentVariableDisplayName.Value = mEquipmentVariable.EquipmentVariableDisplayName;
                //Equipment Variable Display Name
                txtEditEquipmentVariableDisplayName.Text = mEquipmentVariable.EquipmentVariableDisplayName;
                //User Editable
                chkEditUserEditable.Checked = mEquipmentVariable.UserEditable;
                //IP Default Value
                txtEditIPDefaultValue.Text = mEquipmentVariable.IPDefaultValueCultureFormatted;
                //SI Default Value
                txtEditSIDefaultValue.Text = mEquipmentVariable.SIDefaultValueCultureFormatted;

                //Equipment Variable Description
                txtEditDescription.Value = String.IsNullOrEmpty(mEquipmentVariable.EquipmentVariableDescription) ? null : mEquipmentVariable.EquipmentVariableDescription;

                //IP Eng Units
                ddlEditIPEngUnits.SelectedValue = Convert.ToString(mEquipmentVariable.IPEngUnitID);
                //SI Eng Units
                ddlEditSIEngUnits.SelectedValue = Convert.ToString(mEquipmentVariable.SIEngUnitID);

                SetupEngUnitTextBoxes(false);
                SetEngUnitInfo(false);
                SetValidationFailureState(false, "", "");
            }

            #endregion

        #region Load and Bind Fields

            private System.Text.StringBuilder mText = null;
            private void BindAllEngUnitsDropdowns()
            {
                ddlAddIPEngUnits.DataTextField = ddlAddSIEngUnits.DataTextField = ddlEditIPEngUnits.DataTextField = ddlEditSIEngUnits.DataTextField = "EngUnits";
                ddlAddIPEngUnits.DataValueField = ddlAddSIEngUnits.DataValueField = ddlEditIPEngUnits.DataValueField = ddlEditSIEngUnits.DataValueField = "EngUnitID";
                ddlAddIPEngUnits.DataSource = ddlAddSIEngUnits.DataSource = ddlEditIPEngUnits.DataSource = ddlEditSIEngUnits.DataSource = EngUnitsHelper.EngUnits;

                ddlAddIPEngUnits.DataBind();
                ddlAddSIEngUnits.DataBind();
                ddlEditIPEngUnits.DataBind();
                ddlEditSIEngUnits.DataBind();
                mText = new System.Text.StringBuilder();
                mText.AppendLine("<script>");
                mText.AppendLine("var engUnitInfo = new Object();");
                EngUnitsHelper.ForEachEngUnit(OnEngUnit);
                mText.AppendLine("</script>");
                ClientScript.RegisterStartupScript(GetType(), "engUnitsInfo", mText.ToString());
                ddlEditIPEngUnits.Attributes["onchange"] = "OnEngUnitChange(this, '" + spEditIPInfo.ClientID + "', '" + txtEditIPDefaultValue.ClientID + "', '" + valFailureEditIPDefaultValue.ClientID + "' );";
                ddlEditSIEngUnits.Attributes["onchange"] = "OnEngUnitChange(this, '" + spEditSIInfo.ClientID + "', '" + txtEditSIDefaultValue.ClientID + "', '" + valFailureEditSIDefaultValue.ClientID + "' );";
                ddlAddIPEngUnits.Attributes["onchange"] = "OnEngUnitChange(this, '" + spAddIPInfo.ClientID + "', '" + txtAddIPDefaultValue.ClientID + "', '" + valFailureAddIPDefaultValue.ClientID + "' );";
                ddlAddSIEngUnits.Attributes["onchange"] = "OnEngUnitChange(this, '" + spAddSIInfo.ClientID + "', '" + txtAddSIDefaultValue.ClientID + "', '" + valFailureAddSIDefaultValue.ClientID + "' );";
            }

            private void OnEngUnit(EngUnit engUnit)
            {
                bool isStringOrArray = false;
                switch (engUnit.EngUnits.ToUpper())
                {
                    case BusinessConstants.EngUnit.EngineeringUnitArray:
                    case BusinessConstants.EngUnit.EngineeringUnitString:
                        {
                            isStringOrArray = true;
                            break;
                        }
                }
                mText.AppendLine("engUnitInfo['" + engUnit.EngUnitID + "'] = {engUnit:'" + EngUnitsHelper.CreateEngUnitInfo(engUnit) + "', isStringOrArray: " + isStringOrArray.ToString().ToLower() + "};");
            }
        #endregion

        #region Load Equipment Variable

            protected void LoadAddFormIntoEquipmentVariable(EquipmentVariable equipmentVariable)
            {
                //Equipment Variable Name
                equipmentVariable.EquipmentVariableName = txtAddEquipmentVariableName.Text;
                //Equipment Variable Display Name
                equipmentVariable.EquipmentVariableDisplayName = txtAddEquipmentVariableDisplayName.Text;
                //User Editable
                equipmentVariable.UserEditable = chkAddUserEditable.Checked;
                //Equipment Variable Description
                equipmentVariable.EquipmentVariableDescription = String.IsNullOrEmpty(txtAddDescription.Value) ? null : txtAddDescription.Value;
                //IP Eng Units
                equipmentVariable.IPEngUnitID = Convert.ToInt32(ddlAddIPEngUnits.SelectedValue);
                //SI Eng Units
                equipmentVariable.SIEngUnitID = Convert.ToInt32(ddlAddSIEngUnits.SelectedValue);
                //IP Default Value
                equipmentVariable.IPDefaultValue = txtAddIPDefaultValue.Text;

                //SI Default Value
                equipmentVariable.SIDefaultValue = txtAddSIDefaultValue.Text;

                equipmentVariable.DateModified = DateTime.UtcNow;
                SetupEngUnitTextBoxes();
            }

            protected void LoadEditFormIntoEquipmentVariable(EquipmentVariable equipmentVariable)
            {
                //ID
                equipmentVariable.EVID = Convert.ToInt32(hdnEditID.Value);
                //Equipment Variable Name
                equipmentVariable.EquipmentVariableName = txtEditEquipmentVariableName.Text;
                //Equipment Variable Display Name
                equipmentVariable.EquipmentVariableDisplayName = txtEditEquipmentVariableDisplayName.Text;
                //User Editable
                equipmentVariable.UserEditable = chkEditUserEditable.Checked;
                //Equipment Variable Description
                equipmentVariable.EquipmentVariableDescription = String.IsNullOrEmpty(txtEditDescription.Value) ? null : txtEditDescription.Value;
                //IP Eng Units
                equipmentVariable.IPEngUnitID = Convert.ToInt32(ddlEditIPEngUnits.SelectedValue);
                //SI Eng Units
                equipmentVariable.SIEngUnitID = Convert.ToInt32(ddlEditSIEngUnits.SelectedValue);

                //IP Default Value
                equipmentVariable.IPDefaultValue = txtEditIPDefaultValue.Text;

                //SI Default Value
                equipmentVariable.SIDefaultValue = txtEditSIDefaultValue.Text;

                equipmentVariable.DateModified = DateTime.UtcNow;
                SetupEngUnitTextBoxes(false);
            }

        #endregion
         
        #region Button Events

            /// <summary>
            /// Add equipment variable Button on click.
            /// </summary>
            protected void addEquipmentVariableButton_Click(object sender, EventArgs e)
            {
                mEquipmentVariable = new EquipmentVariable();

                //load the form into the equipment variable
                LoadAddFormIntoEquipmentVariable(mEquipmentVariable);

                //check if ip or si is string, csv, or bool and make sure they both are the same
                if ((mEquipmentVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitStringID || mEquipmentVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitArrayID || mEquipmentVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitBoolID || mEquipmentVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitStringID || mEquipmentVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitArrayID || mEquipmentVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitBoolID) && mEquipmentVariable.IPEngUnitID != mEquipmentVariable.SIEngUnitID)
                {
                    lblAddError.Text = equipmentVariableStringOrBool;
                    lblAddError.Visible = true;
                    lnkSetFocusAdd.Focus();
                }
                //check if equipment variable name already exists
                else if (DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableNameExist(mEquipmentVariable.EquipmentVariableName))
                {
                    lblAddError.Text = addEquipmentVariableNameExists;
                    lblAddError.Visible = true;
                    lnkSetFocusAdd.Focus();                  
                }
                //check if equipment variable display name already exists
                else if (DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableDisplayNameExist(mEquipmentVariable.EquipmentVariableDisplayName))
                {
                    lblAddError.Text = addEquipmentVariableDisplayNameExists;
                    lblAddError.Visible = true;
                    lnkSetFocusAdd.Focus();
                }
                else
                {
                    try
                    {
                        SetValidationFailureState(true, "", "");
                        //insert new equipment variable
                        DataMgr.EquipmentVariableDataMapper.InsertEquipmentVariable(mEquipmentVariable);

                        lblAddError.Text = mEquipmentVariable.EquipmentVariableName + addEquipmentVariableSuccess;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                    catch (CW.Data.Exceptions.ValidationException ex)
                    {
                        // We'll have 2 but 1 may be blank
                        SetValidationFailureState(true, ex.Errors[0], ex.Errors[1]);
                        lblEditError.Text = updateFailedCorrectErrors;

                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment variable." + ex.FlattenErrors);
                    }
                    catch (SqlException sqlEx)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment variable.", sqlEx);
                        lblAddError.Text = addEquipmentVariableFailed;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding equipment variable.", ex);
                        lblAddError.Text = addEquipmentVariableFailed;
                        lblAddError.Visible = true;
                        lnkSetFocusAdd.Focus();
                    }
                }

                SetEngUnitInfo();
            }

            private void SetValidationFailureState(bool bAdd, string ipMsg, string siMsg)
            {
                (bAdd ? valFailureAddIPDefaultValue : valFailureEditIPDefaultValue).InnerHtml = ipMsg;
                (bAdd ? valFailureAddIPDefaultValue : valFailureEditIPDefaultValue).Style[HtmlTextWriterStyle.Display] = string.IsNullOrEmpty(ipMsg) ? "none" : "block";
                (bAdd ? valFailureAddSIDefaultValue : valFailureEditSIDefaultValue).InnerHtml = siMsg;
                (bAdd ? valFailureAddSIDefaultValue : valFailureEditSIDefaultValue).Style[HtmlTextWriterStyle.Display] = string.IsNullOrEmpty(siMsg) ? "none" : "block";
            }

            private void SetEngUnitInfo(bool bAdd = true)
            {
                (bAdd ? spAddIPInfo : spEditIPInfo).InnerHtml = EngUnitsHelper.CreateEngUnitInfo(mEquipmentVariable.IPEngUnitID);
                (bAdd ? spAddSIInfo : spEditSIInfo).InnerHtml = EngUnitsHelper.CreateEngUnitInfo(mEquipmentVariable.SIEngUnitID);
            }

            private void SetupEngUnitTextBoxes(bool bAdd = true)
            {
                (new[] { new { engunitid = mEquipmentVariable.IPEngUnitID, textbox = (bAdd ? txtAddIPDefaultValue: txtEditIPDefaultValue) },
                        new { engunitid = mEquipmentVariable.SIEngUnitID, textbox = (bAdd ? txtAddSIDefaultValue: txtEditSIDefaultValue) } 
                       }
                       ).ForEach(pair =>
                       {
                           ControlHelper.SetTextBoxSingleMultiple(pair.engunitid, pair.textbox);
                       });
            }            /// <summary>
            /// Update equipment variable Button on click. Updates equipment variable data.
            /// </summary>
            protected void updateEquipmentVariableButton_Click(object sender, EventArgs e)
            {
                mEquipmentVariable = new EquipmentVariable();

                //load the form into the equipment variable
                LoadEditFormIntoEquipmentVariable(mEquipmentVariable);

                //check if ip or si is string, csv, or bool and make sure they both are the same
                if ((mEquipmentVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitStringID || mEquipmentVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitArrayID || mEquipmentVariable.IPEngUnitID == BusinessConstants.EngUnit.EngineeringUnitBoolID || mEquipmentVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitStringID || mEquipmentVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitArrayID || mEquipmentVariable.SIEngUnitID == BusinessConstants.EngUnit.EngineeringUnitBoolID) && mEquipmentVariable.IPEngUnitID != mEquipmentVariable.SIEngUnitID)
                {
                    lblEditError.Text = equipmentVariableStringOrBool;
                }
                //check if trying to change equipment variable name, and check if it already exists.
                else if (mEquipmentVariable.EquipmentVariableName != hdnEquipmentVariableName.Value && (DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableNameExist(mEquipmentVariable.EquipmentVariableName)))
                {
                    lblEditError.Text = updateEquipmentVariableNameExists;
                }
                //check if trying to change equipment variable display name, and check if it already exists.
                else if (mEquipmentVariable.EquipmentVariableDisplayName != hdnEquipmentVariableDisplayName.Value && (DataMgr.EquipmentVariableDataMapper.DoesEquipmentVariableDisplayNameExist(mEquipmentVariable.EquipmentVariableDisplayName)))
                {
                    lblEditError.Text = updateEquipmentVariableDisplayNameExists;
                }                
                else
                {
                    //try to update the equipment variables              
                    try
                    {
                        SetValidationFailureState(false, "", "");
                        DataMgr.EquipmentVariableDataMapper.UpdateEquipmentVariable(mEquipmentVariable);

                        lblEditError.Text = updateSuccessful;

                        //Bind equipment variables again
                        BindEquipmentVariables();
                    }
                    catch (CW.Data.Exceptions.ValidationException ex)
                    {
                        // We'll always have 2 but 1 may be blank
                        SetValidationFailureState(false, ex.Errors[0], ex.Errors[1]);
                        lblEditError.Text = updateFailedCorrectErrors;

                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment variable." + ex.FlattenErrors);
                    }
                    catch (Exception ex)
                    {
                        lblEditError.Text = updateFailed;

                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating equipment variable.", ex);
                    }
                }

                SetEngUnitInfo(false);

                lblEditError.Visible = true;
                lnkSetFocusEdit.Focus();
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindEquipmentVariables();
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                txtSearch.Text = "";
                sessionState["Search"] = String.Empty;
                BindEquipmentVariables();
            }

        #endregion

        #region Dropdown events

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind equipment variable grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                //if active tab index is 0, bind equipment variables to grid
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                {
                    BindEquipmentVariables();
                }
            }

        #endregion

        #region Grid Events

            protected void gridEquipmentVariables_OnDataBound(object sender, EventArgs e)
            {                             
            }

            protected void gridEquipmentVariables_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                pnlEditEquipmentVariable.Visible = false;
                dtvEquipmentVariable.Visible = true;
                
                int evid = Convert.ToInt32(gridEquipmentVariables.DataKeys[gridEquipmentVariables.SelectedIndex].Values["EVID"]);

                //set data source
                dtvEquipmentVariable.DataSource = new List<GetEquipmentVariableData>() { DataMgr.EquipmentVariableDataMapper.GetFullEquipmentVariableByEVID(evid) };
                //bind equipment variable to details view
                dtvEquipmentVariable.DataBind();
            }

            protected void gridEquipmentVariables_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridEquipmentVariables_Editing(object sender, GridViewEditEventArgs e)
            {
                dtvEquipmentVariable.Visible = false;
                pnlEditEquipmentVariable.Visible = true;

                int evid = Convert.ToInt32(gridEquipmentVariables.DataKeys[e.NewEditIndex].Values["EVID"]);

                mEquipmentVariable = DataMgr.EquipmentVariableDataMapper.GetEquipmentVariable(evid);

                //Set Equipment Variable data
                SetEquipmentVariableIntoEditForm();

                //Cancels the edit auto grid selected.
                e.Cancel = true;
            }

            protected void gridEquipmentVariables_Deleting(object sender, GridViewDeleteEventArgs e)
            {
                //get deleting rows equipmentVariableid
                int evid = Convert.ToInt32(gridEquipmentVariables.DataKeys[e.RowIndex].Value);
                int top = 10;

                try
                {
                    //Only allow deletion of a equipment variable if it as not been assocaited with
                    //a piece of equipment or an analysis.  
                  
                    //check if equipment variable is associated to any equipment
                    List<GetEquipmentData> equipment = DataMgr.EquipmentDataMapper.GetTopEquipmentAssociatedWithEVID(evid, top);

                    if (equipment != null)
                    {
                        lblErrors.Text = associatedToEquipment;

                        lblErrors.Text += " The " + (equipment.Count < top ? equipment.Count.ToString() : "first " + top.ToString()) + " associated equipment:<br />";
                        
                        foreach (GetEquipmentData eq in equipment)
                        {
                            lblErrors.Text += "<br />" + eq.BuildingName + " - " + eq.EquipmentName;
                        }

                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    //check if equipment variable is associated to any analysis
                    else if (DataMgr.EquipmentVariableDataMapper.IsEquipmentVariableAssociatedToAnyAnalysis(evid))
                    {
                        lblErrors.Text = associatedToAnalysis;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    //check if equipment variable is associated to any equipment class
                    else if (DataMgr.EquipmentVariableDataMapper.IsEquipmentVariableAssociatedToEquipmentClass(evid))
                    {
                        lblErrors.Text = associatedToEquipmentClass;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }
                    else
                    {
                        //delete equipment variable
                        DataMgr.EquipmentVariableDataMapper.DeleteEquipmentVariableByEVID(evid);

                        lblErrors.Text = deleteSuccessful;
                        lblErrors.Visible = true;
                        lnkSetFocusView.Focus();
                    }  
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error deleting equipment variable.", ex);
                }

                //Bind data again keeping current page and sort mode
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryEquipmentVariables());
                gridEquipmentVariables.PageIndex = gridEquipmentVariables.PageIndex;
                gridEquipmentVariables.DataSource = SortDataTable(dataTable as DataTable, true);
                gridEquipmentVariables.DataBind();

                SetGridCountLabel(dataTable.Rows.Count);

                //hide edit form if shown
                pnlEditEquipmentVariable.Visible = false;  
            }

            protected void gridEquipmentVariables_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEquipmentVariables(sessionState["Search"].ToString()));

                //maintain current sort direction and expresstion on paging
                gridEquipmentVariables.DataSource = SortDataTable(dataTable, true);
                gridEquipmentVariables.PageIndex = e.NewPageIndex;
                gridEquipmentVariables.DataBind();
            }

            protected void gridEquipmentVariables_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridEquipmentVariables.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedEquipmentVariables(sessionState["Search"].ToString()));

                GridViewSortExpression = e.SortExpression;

                gridEquipmentVariables.DataSource = SortDataTable(dataTable, false);
                gridEquipmentVariables.DataBind();
            }
        
        #endregion

        #region Helper Methods

            private IEnumerable<GetEquipmentVariableData> QueryEquipmentVariables()
            {
                try
                {
                    //get all equipment variables 
                    return DataMgr.EquipmentVariableDataMapper.GetAllFullEquipmentVariables();
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment variables.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment variables.", ex);
                    return null;
                }
            }

            private IEnumerable<GetEquipmentVariableData> QuerySearchedEquipmentVariables(string searchText)
            {
                try
                {
                    //get all equipment variables 
                    return DataMgr.EquipmentVariableDataMapper.GetAllSearchedFullEquipmentVariables(searchText);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment variables.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving equipment variables.", ex);
                    return null;
                }
            }

            private void BindEquipmentVariables()
            {
                //query equipment variables
                IEnumerable<GetEquipmentVariableData> variables = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryEquipmentVariables() : QuerySearchedEquipmentVariables(txtSearch.Text);

                int count = variables.Count();

                gridEquipmentVariables.DataSource = variables;

                // bind grid
                gridEquipmentVariables.DataBind();

                SetGridCountLabel(count);
            }

            //private void BindSearchedEquipmentVariables(string[] searchText)
            //{
            //    //query equipment variables
            //    IEnumerable<GetEquipmentVariableData> variables = QuerySearchedEquipmentVariables(searchText);

            //    int count = variables.Count();

            //    gridEquipmentVariables.DataSource = variables;

            //    // bind grid
            //    gridEquipmentVariables.DataBind();

            //    SetGridCountLabel(count);
            //}

            private void SetGridCountLabel(int count)
            {
                //check if grid empty 
                if (count == 1)
                {
                    lblResults.Text = String.Format("{0} equipment variable found.", count);
                }
                else if (count > 1)
                {
                    lblResults.Text = String.Format("{0} equipment variables found.", count);
                }
                else
                {
                    //gridCompanies.EmptyDataText
                    lblResults.Text = "No equipment variables found.";
                }
            }

            /// <summary>
            /// Helper method to switch sort direction
            /// </summary>
            /// <returns>Alternated Sort Direction</returns>
            private string GetSortDirection()
            {
                //switch sorting directions
                switch (GridViewSortDirection)
                {
                    case "ASC":
                        GridViewSortDirection = "DESC";
                        ViewState["SortDirection"] = "DESC";
                        break;
                    case "DESC":
                        GridViewSortDirection = "ASC";
                        ViewState["SortDirection"] = "ASC";
                        break;
                }
                return GridViewSortDirection;
            }

            /// <summary>
            /// Sorts the data table based on view state.
            /// </summary>
            /// <param name="dataTable"></param>
            /// <param name="isPageIndexChanging"></param>
            /// <returns>DataView</returns>
            protected DataView SortDataTable(DataTable dataTable, bool isPageIndexChanging)
            {
                if (dataTable != null && dataTable.Rows.Count != 0)
                {
                    DataView dataView = new DataView(dataTable);
                    if (GridViewSortExpression != string.Empty)
                    {
                        if (isPageIndexChanging)
                        {
                            //maintains the currect viewstate sorting for paging
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GridViewSortDirection);
                        }
                        else
                        {
                            //reverses the sort direction on sort
                            dataView.Sort = string.Format("{0} {1}", GridViewSortExpression, GetSortDirection());
                        }
                    }
                    return dataView;
                }
                else
                {
                    return new DataView();
                }
            }        

        #endregion
    }
}

