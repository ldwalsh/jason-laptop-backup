﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemLogs.aspx.cs" Inherits="CW.Website.SystemLogs" %>
<%@ Register Assembly="Telerik.Web.UI" TagPrefix="telerik" Namespace="Telerik.Web.UI" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

    <script type="text/javascript" src="_assets/scripts/dates.js"></script>

             <h1>
                 System Logs</h1>
             <div class="richText">
                The system log page is designed to select and view a log file in the system.
             </div>
             <div class="updateProgressDiv">
                 <asp:UpdateProgress ID="updateProgressTop" runat="server">
                     <ProgressTemplate>
                            <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                        </ProgressTemplate>
                 </asp:UpdateProgress>
             </div>
             <asp:Panel ID="pnlSubmit" runat="server" DefaultButton="btnSubmit"> 
             <div>
                 <p>
                     <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>
                 </p>
                 <div class="divFormWide">
                     <label class="labelNarrow">
                     Log Section:</label>
                     <asp:DropDownList CssClass="dropdown" ID="ddlLogSection" AppendDataBoundItems="true" runat="server">                        
                        <asp:ListItem Value="-1">Select one...</asp:ListItem>                            
                     </asp:DropDownList>

                     <asp:RequiredFieldValidator ID="sectionRequiredValidator" runat="server"
                        ErrorMessage="Section is a required field." 
                        ControlToValidate="ddlLogSection"  
                        SetFocusOnError="true" 
                        Display="None" 
                        InitialValue="-1"
                        ValidationGroup="SystemLogs">
                        </asp:RequiredFieldValidator>
                     <ajaxToolkit:ValidatorCalloutExtender ID="sectionRequiredValidatorExtender" runat="server"
                        BehaviorID="sectionRequiredValidatorExtender" 
                        TargetControlID="sectionRequiredValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>    
                 </div>
                 <div class="divFormWide">
                     <label class="labelNarrow">
                     Log Level:</label>
                     <asp:DropDownList CssClass="dropdown" ID="ddlLogLevel" AppendDataBoundItems="true" runat="server">
                        <asp:ListItem Value="-1">Select one...</asp:ListItem>     
                        <asp:ListItem Value="ALL">ALL</asp:ListItem>      
                     </asp:DropDownList>

                     <asp:RequiredFieldValidator ID="levelRequiredValidator" runat="server"
                        ErrorMessage="Level is a required field." 
                        ControlToValidate="ddlLogLevel"  
                        SetFocusOnError="true" 
                        Display="None" 
                        InitialValue="-1"
                        ValidationGroup="SystemLogs">
                        </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="levelRequiredValidatorExtender" runat="server"
                        BehaviorID="levelRequiredValidatorExtender" 
                        TargetControlID="levelRequiredValidator"
                        HighlightCssClass="validatorCalloutHighlight"
                        Width="175">
                        </ajaxToolkit:ValidatorCalloutExtender>    
                 </div>
                 <div class="divFormWide">
                    <div class="quickDates">Prior:
                      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 48, 'hours', true);">2 days</a>
                      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 24, 'hours', true);">1 day</a>
                      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 12, 'hours', true);">12 hrs</a>
                      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 6, 'hours', true);">6 hrs</a>
                      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 1, 'hours', true);">1 hr</a>
                      <a href="javascript:" onclick="PopulateRadDatePickerDates('<%= txtStartDate.ClientID %>', '<%= txtEndDate.ClientID %>', 10, 'minutes', true);">10 mins</a>
                    </div>
                </div>
                <div class="divFormWide">
                    <label class="labelNarrow">Start Date (UTC):</label>
                    <telerik:RadDateTimePicker ID="txtStartDate" runat="server"></telerik:RadDateTimePicker>                     
                    <asp:RequiredFieldValidator ID="dateStartRequiredValidator" runat="server"
                                        ErrorMessage="Date is a required field."
                                        ControlToValidate="txtStartDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="SystemLogs"
                                        >
                                        </asp:RequiredFieldValidator>
                   <ajaxToolkit:ValidatorCalloutExtender ID="dateStartRequiredValidatorExtender" runat="server"
                                        BehaviorID="dateStartRequiredValidatorExtender"
                                        TargetControlID="dateStartRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                          
                </div>
                <div class="divFormWide">
                    <label class="labelNarrow">End Date (UTC):</label>
                    <telerik:RadDateTimePicker ID="txtEndDate" runat="server"></telerik:RadDateTimePicker>                      
                    <asp:RequiredFieldValidator ID="dateEndRequiredValidator" runat="server"
                                        ErrorMessage="End date is a required field."
                                        ControlToValidate="txtEndDate"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="SystemLogs">
                                        </asp:RequiredFieldValidator>
                    <ajaxToolkit:ValidatorCalloutExtender ID="dateEndRequiredValidatorExtender" runat="server"
                                        BehaviorID="dateEndRequiredValidatorExtender"
                                        TargetControlID="dateEndRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                       
                    <asp:CustomValidator ID="dateCustomValidator" runat="server" ClientValidationFunction="RadDatePickerDatetimeComparer" ControlToValidate="txtEndDate" EnableClientScript="true" ErrorMessage="End Date cannot be before Start Date" Display="None" ValidationGroup="SystemLogs" />
                    <ajaxToolkit:ValidatorCalloutExtender ID="dateValidatorCalloutExtender" runat="server" BehaviorID="dateValidatorCalloutExtender" TargetControlID="dateCustomValidator" HighlightCssClass="validatorCalloutHighlight" Width="175" />
                </div>
                <div class="divFormWide">
                     <asp:LinkButton CssClass="lnkButton" ValidationGroup="SystemLogs" runat="server" Text="Submit" ID="btnSubmit" onclick="btnSubmit_Click" />
                 </div>
                 <div class="divFormWide">
                     <label class="labelNarrow">
                     Log Count:</label>
                     <asp:Label ID="lblCount" CssClass="labelContent" runat="server">                                                        
                    </asp:Label>
                 </div>
                 <div class="divFormWide">
                     <label class="labelNarrow">
                     Log Text:</label>
                     <div id="logDiv" class="divLogs" runat="server">         
                        <asp:Literal ID="logText" runat="server"></asp:Literal>                                               
                     </div>
                 </div>
             </div>
             </asp:Panel>

</asp:Content>                