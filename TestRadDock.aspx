﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TestRadDock.aspx.cs" Inherits="CW.Website.TestRadDock" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
       

        
    <div>

         <telerik:RadScriptManager ID="RadScriptManager" runat="server" />
        <script type="text/javascript">

            function Test() {
                //if (radDockBuildingMap.ClientID != null) {
                    var dock = $find('<%= radDockBuildingMap.ClientID%>');
                    var content = dock.get_contentContainer();
                     var text = $telerik.isIE ? content.childNodes[0].innerHTML : content.childNodes[1].innerHTML;
                     alert($telerik.isIE + " " + content.childNodes[0].innerHTML);
               // }
              //  else
               //     alert("raddock is null");
            }
        </script>

    <telerik:RadDockLayout ID="radDockLayout1" runat="server">

    <telerik:RadDockZone ID="radDockZone1" BorderStyle="None" runat="server" FitDocks="false" Orientation="Horizontal">
                        
      <telerik:RadDock ID="radDockBuildingMap" DefaultCommands="ExpandCollapse" EnableAnimation="true" Title="Building Map" CssClass="radDock" runat="server" EnableRoundedCorners="true" DockMode="Docked" Resizable="false" Width="438px">
        <ContentTemplate>
          <asp:UpdatePanel ID="udpBuildingMap" runat="server">
            <ContentTemplate>
		      <div id="injectHere"><div id="Div1">inner test</div>test</div>
            </ContentTemplate>
          </asp:UpdatePanel>
	    </ContentTemplate>
      </telerik:RadDock>

    </telerik:RadDockZone>

  </telerik:RadDockLayout>
        <input type="button" value="???" onclick="Test()" />
    </div>
    </form>
</body>
</html>