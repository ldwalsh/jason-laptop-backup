﻿<%@ Page Language="C#" MasterPageFile="~/_masters/KGSAdmin.Master" AutoEventWireup="false" CodeBehind="KGSUploadBuildingVariables.aspx.cs" Inherits="CW.Website.KGSUploadBuildingVariables" %>
<%@ Register src="~/_controls/upload/UploadHeader.ascx" tagname="UploadHeader" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/UploadFormat.ascx" tagname="UploadFormat" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/IDLookups/IDLookupsBuildingVariableUpload.ascx" tagname="IDLookupsBuildingVariableUpload" tagprefix="CW" %>
<%@ Register src="~/_controls/upload/Upload.ascx" tagname="Upload" tagprefix="CW" %>

<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">

  <CW:UploadHeader ID="UploadHeader" runat="server" />
  <CW:UploadFormat ID="UploadFormat" runat="server" />
  <CW:IDLookupsBuildingVariableUpload ID="IDLookupsBuildingVariableUpload" runat="server" />
  <CW:Upload ID="Upload" runat="server" />

</asp:Content>