﻿<%@ Page Language="C#" AutoEventWireup="true" %>
<script runat=server>
    protected String GetJSLibraryURL(int level)
    {
      var key = Request.QueryString["key"];
      switch(key)
      {
        case "jq":
          return "https://code.jquery.com/jquery-2.1.4.min.js";
        case "jq-g":
          return "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js";
        case "jq-local":
          return "/_assets/scripts/jquery-2.1.4.min.js";
        case "jq-1.8.3":
          return "https://code.jquery.com/jquery-1.8.3.min.js";
        case "jq-1.8.3-local":
          return "/_assets/scripts/jquery-1.8.3.min.js";
        case "jq-g-1.8.3":
          return "https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js";
        case "jq-ui":
          return level == 0 ? "https://code.jquery.com/jquery-2.1.4.min.js" : "https://code.jquery.com/ui/1.9.2/jquery-ui.min.js";
        case "jq-ui-g":
          return level == 0 ? "https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js" : "https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js";
        case "jq-ui-local":
          return level == 0 ? "/_assets/scripts/jquery-2.1.4.min.js" : "/_assets/scripts/jquery-ui-1.9.2.min.js";
        case "jq-color":
          return "https://code.jquery.com/color/jquery.color-2.1.2.min.js";
        case "kendo":
          return "https://da7xgjtj801h2.cloudfront.net/2015.1.318/js/kendo.core.min.js";
        case "bing-map":
          return "https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&s=1";
        case "highStock":
          return "https://code.highcharts.com/stock/highstock.js";
        case "highStockExport":
          return level == 0 ? "https://code.highcharts.com/stock/highstock.js" : "https://code.highcharts.com/stock/modules/exporting.js";
        case "jsapi":
          return "https://www.google.com/jsapi";
        default:
          return "unkown";
      }
    }
</script>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script type="text/javascript" src="<%=GetJSLibraryURL(0)%>"></script>
    <script type="text/javascript" src="<%=GetJSLibraryURL(1)%>"></script>
</head>
<body>
</body>
</html>
