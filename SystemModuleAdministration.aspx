﻿<%@ Page Language="C#" MasterPageFile="~/_masters/SystemAdmin.master" EnableViewState="true" AutoEventWireup="false" CodeBehind="SystemModuleAdministration.aspx.cs" Inherits="CW.Website.SystemModuleAdministration" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register TagPrefix="telerik" Namespace="Telerik.Web.UI" Assembly="Telerik.Web.UI" %>
<%@ Import Namespace="CW.Utility" %>
<asp:Content ID="plcCopyContent" ContentPlaceHolderID="plcCopy" runat="server">
                      	                  	
            	<h1>System Module Administration</h1>                        
                <div class="richText">The system module administration area is used to view, add, and edit modules.</div>                                                 
                <div class="updateProgressDiv">
                        <asp:UpdateProgress ID="updateProgressTop" runat="server">
                            <ProgressTemplate>                                     
                                <img src="_assets/images/Pik4F3300.gif" alt="loading" /><span>Loading...</span>
                            </ProgressTemplate>
                        </asp:UpdateProgress>  
                </div> 
                <div class="administrationControls">
                        <telerik:RadTabStrip ID="radTabStrip" runat="server" SelectedIndex="0" OnTabClick="onTabClick" AutoPostBack="true" MultiPageID="radMultiPage"> 
                        <Tabs>
                            <telerik:RadTab Text="View Modules"></telerik:RadTab>
                            <telerik:RadTab Text="Add Module"></telerik:RadTab>
                        </Tabs>
                        </telerik:RadTabStrip>
                        <telerik:RadMultiPage ID="radMultiPage" CssClass="radMultiPage" runat="server" SelectedIndex="0">
                            <telerik:RadPageView ID="RadPageView1" runat="server">
                                    <h2>View Modules</h2>
                                    <p>
                                            <a id="lnkSetFocusView" href="#" runat="server"></a>
                                            <asp:Label ID="lblResults" runat="server" Text=""></asp:Label>
                                            <asp:Label ID="lblErrors" CssClass="errorMessage"  runat="server" Visible="false"></asp:Label>                                                                                        
                                    </p> 
                                    <asp:Panel ID="pnlSearch" runat="server" DefaultButton="searchBtn" CssClass="divSearch">
                                        <asp:LinkButton id="viewAll" Text="View All" runat="server" CssClass="viewAll" OnClick="viewAll_Click"></asp:LinkButton>
                                        <asp:TextBox ID="txtSearch" runat="server" CssClass="textbox"></asp:TextBox>
                                        <asp:LinkButton CssClass="lnk-search-button" ID="searchBtn" runat="server" ValidationGroup="Search" OnClick="searchButton_Click"/>   
                                    </asp:Panel>         
                                      
                                    <div id="gridTbl">                                        
                                           <asp:GridView 
                                             ID="gridModules"   
                                             EnableViewState="true"           
                                             runat="server"  
                                             DataKeyNames="ModuleID" 
                                             GridLines="None"                                           
                                             PageSize="20" PagerSettings-PageButtonCount="20"
                                             OnRowCreated="gridModules_OnRowCreated" 
                                             AllowPaging="true"  OnPageIndexChanging="gridModules_PageIndexChanging"
                                             AllowSorting="true"  OnSorting="gridModules_Sorting"                                                                                                            
                                             OnRowEditing="gridModules_Editing"
                                             OnDataBound="gridModules_OnDataBound"
                                             AutoGenerateColumns="false"                                              
                                             HeaderStyle-CssClass="tblTitle" 
                                             RowStyle-CssClass="tblCol1"
                                             AlternatingRowStyle-CssClass="tblCol2" 
                                             RowStyle-Wrap="true"                                                                                      
                                             > 
                                             <Columns> 
                                                <asp:TemplateField>
                                                    <ItemTemplate>                                                
                                                         <asp:LinkButton ID="LinkButton1" Runat="server" CausesValidation="false" CommandName="Edit">Edit</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="ModuleName" HeaderText="Module Name">  
                                                    <ItemTemplate><label title="<%# Eval("ModuleName") %>"><%# StringHelper.TrimText(Eval("ModuleName"),30) %></label></ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Description">   
                                                    <ItemTemplate><label title="<%# Eval("ModuleDescription") %>"><%# StringHelper.TrimText(Eval("ModuleDescription"), 200) %></label></ItemTemplate>                          
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" HeaderText="Link">  
                                                    <ItemTemplate><label title="<%# Eval("ModuleLink") %>"><%# StringHelper.TrimText(Eval("ModuleLink"),30) %></label></ItemTemplate>                      
                                                </asp:TemplateField>  
                                                <asp:TemplateField ItemStyle-Wrap="true" SortExpression="SortOrder" HeaderText="Sort Order">  
                                                    <ItemTemplate><%# Eval("SortOrder")%></ItemTemplate>
                                                </asp:TemplateField> 
                                                <asp:TemplateField SortExpression="IsActive" ItemStyle-Wrap="true" HeaderText="Active">  
                                                    <ItemTemplate><%# Eval("IsActive")%></ItemTemplate>                                                  
                                                </asp:TemplateField>                                                                                                                                                                                                                                                                    
                                             </Columns>        
                                         </asp:GridView>                                      
                                    </div>
                                    <br />
                                    
                                    <!--EDIT MODULE PANEL -->                              
                                    <asp:Panel ID="pnlEditModule" runat="server" Visible="false" DefaultButton="btnUpdateModule">                                                                                             
                                        <div>
                                            <h2>Edit Module</h2>
                                        </div>  
                                        <div>                                                    
                                            <asp:Label ID="lblEditError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                            <asp:HiddenField ID="hdnEditID" runat="server" Visible="false" />
                                            <div class="divForm">
                                                <label class="label">*Module Name:</label>
                                                <asp:TextBox ID="txtEditModuleName" MaxLength="50" runat="server"></asp:TextBox>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">Description:</label>
                                                <textarea name="txtEditDescription" id="txtEditDescription" cols="40" onkeyup="limitChars(this, 500, 'divEditDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                                <div id="divEditDescriptionCharInfo"></div>
                                            </div>      
                                            <div class="divForm">
                                                <label class="label">*Link:</label>
                                                <asp:TextBox ID="txtEditModuleLink" MaxLength="50" runat="server"></asp:TextBox>                                      
                                            </div> 
                                            <div class="divForm">
                                                <label class="label">*Sort Order:</label>
                                                <asp:TextBox ID="txtEditModuleSortOrder" MaxLength="2" runat="server"></asp:TextBox>                                  
                                            </div>  
                                            <div class="divForm">
                                                <label class="label">*Active:</label>
                                                <asp:CheckBox ID="chkEditActive" CssClass="checkbox" runat="server" />                                                        
                                                <p>(Warning: Setting a module as inactive will not allow showing of this module, or this modules quicklinks.)</p>
                                            </div>                                                                                                                                                                            
                                            <asp:LinkButton CssClass="lnkButton" ID="btnUpdateModule" runat="server" Text="Update Module"  OnClick="updateModuleButton_Click" ValidationGroup="EditModule"></asp:LinkButton>
                                        </div>
                                    
                                   <!--Ajax Validators-->      
                                   <asp:RequiredFieldValidator ID="editModuleNameRequiredValidator" runat="server"
                                        ErrorMessage="Module Name is a required field."
                                        ControlToValidate="txtEditModuleName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditModule">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editModuleNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="editModuleNameRequiredValidatorExtender"
                                        TargetControlID="editModuleNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                   <asp:RequiredFieldValidator ID="editModuleLinkRequiredValidator" runat="server"
                                        ErrorMessage="Module Link is a required field."
                                        ControlToValidate="txtEditModuleLink"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditModule">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editModuleLinkRequiredValidatorExtender" runat="server"
                                        BehaviorID="editModuleLinkRequiredValidatorExtender"
                                        TargetControlID="editModuleLinkRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>             
                                    <ajaxToolkit:FilteredTextBoxExtender ID="editModuleSortOrderFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtEditModuleSortOrder"         
                                            FilterType="Numbers"
                                            />   
                                    <asp:RequiredFieldValidator ID="editModuleSortOrderRequiredValidator" runat="server"
                                        ErrorMessage="Sort Order is a required field."
                                        ControlToValidate="txtEditModuleSortOrder"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="EditModule">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="editModuleSortOrderRequiredValidatorExtender" runat="server"
                                        BehaviorID="editModuleSortOrderRequiredValidatorExtender"
                                        TargetControlID="editModuleSortOrderRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                                    </asp:Panel>
                                                                                     
                            </telerik:RadPageView>
                            <telerik:RadPageView ID="RadPageView2" runat="server">
                                <asp:Panel ID="pnlAddModule" runat="server" DefaultButton="btnAddModule">  
                                    <h2>Add Module</h2>
                                    <div>    
                                        <a id="lnkSetFocusAdd" runat="server"></a>                                                
                                        <asp:Label ID="lblAddError" CssClass="errorMessage" Visible="false" runat="server"></asp:Label>                  
                                        
                                        <div class="divForm">
                                            <label class="label">*Module Name:</label>
                                            <asp:TextBox ID="txtAddModuleName" CssClass="textbox" MaxLength="50" runat="server"></asp:TextBox>                                            
                                        </div>                                        
                                        <div class="divForm">
                                             <label class="label">Description:</label>
                                             <textarea name="txtAddDescription" id="txtAddDescription" cols="40" onkeyup="limitChars(this, 500, 'divAddDescriptionCharInfo')" rows="5" runat="server"></textarea>                                                         
                                             <div id="divAddDescriptionCharInfo"></div>
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">*Link:</label>
                                            <asp:TextBox ID="txtAddModuleLink" MaxLength="50" runat="server"></asp:TextBox>                                      
                                        </div> 
                                        <div class="divForm">
                                            <label class="label">*Sort Order:</label>
                                            <asp:TextBox ID="txtAddModuleSortOrder" MaxLength="2" runat="server"></asp:TextBox>                                  
                                        </div>                                                                                                                                                                    
                                        <asp:LinkButton CssClass="lnkButton" ID="btnAddModule" runat="server" Text="Add Module"  OnClick="addModuleButton_Click" ValidationGroup="AddModule"></asp:LinkButton>
                                    </div>
                                    
                                    <!--Ajax Validators-->      
                                    <asp:RequiredFieldValidator ID="addModuleNameRequiredValidator" runat="server"
                                        ErrorMessage="Module Name is a required field."
                                        ControlToValidate="txtAddModuleName"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddModule">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addModuleNameRequiredValidatorExtender" runat="server"
                                        BehaviorID="addModuleNameRequiredValidatorExtender"
                                        TargetControlID="addModuleNameRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender> 
                                    <asp:RequiredFieldValidator ID="addModuleLinkRequiredValidator" runat="server"
                                        ErrorMessage="Module Link is a required field."
                                        ControlToValidate="txtAddModuleLink"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddModule">
                                    </asp:RequiredFieldValidator>   
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addModuleLinkRequiredValidatorExtender" runat="server"
                                        BehaviorID="addModuleLinkRequiredValidatorExtender"
                                        TargetControlID="addModuleLinkRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>   
                                    <ajaxToolkit:FilteredTextBoxExtender ID="addModuleSortOrderFilteredTextBoxExtender" runat="server"
                                            TargetControlID="txtAddModuleSortOrder"         
                                            FilterType="Numbers"
                                            />   
                                    <asp:RequiredFieldValidator ID="addModuleSortOrderRequiredValidator" runat="server"
                                        ErrorMessage="Sort Order is a required field."
                                        ControlToValidate="txtAddModuleSortOrder"          
                                        SetFocusOnError="true"
                                        Display="None"
                                        ValidationGroup="AddModule">
                                        </asp:RequiredFieldValidator>
                                    <ajaxToolkit:ValidatorCalloutExtender ID="addModuleSortOrderRequiredValidatorExtender" runat="server"
                                        BehaviorID="addModuleSortOrderRequiredValidatorExtender"
                                        TargetControlID="addModuleSortOrderRequiredValidator"
                                        HighlightCssClass="validatorCalloutHighlight"
                                        Width="175">
                                        </ajaxToolkit:ValidatorCalloutExtender>

                                </asp:Panel>              
                            </telerik:RadPageView>                          
                        </telerik:RadMultiPage>
                   </div>                                                                 
          
</asp:Content>


                    
                  
