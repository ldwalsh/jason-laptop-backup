﻿using CW.Common.Constants;
using CW.Data;
using CW.Data.AzureStorage.Models;
using CW.Data.Models.Client;
using CW.Data.Models.User;
using CW.Utility;
using CW.Utility.Web;
using CW.Website._framework;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CW.Website
{
    public partial class KGSUserAdministration : UserAdministrationBase
    {
        #region Properties

            private int globalOID;

        #endregion

        #region Page Events

            protected void Page_Load(object sender, EventArgs e)
            {
                RegisterPostClick(btnUpdateUser, btnUpdateClientRoles, btnUpdateClientBuildings);
                //logged in security check in master page.

                if (!IsPostBack)
                {
                    string map = (hdnOIDToAllowPwd.Value??"").Trim();
                    if( 0 == map.Length)
                    {
                        map = siteUser.UserOID + ":" + (siteUser.IdentityManagedExternalProvider?"1" : "0");
                        if (siteUser.UserOID != siteUser.ClientOID)
                        {
                            map += "," + siteUser.ClientOID + ":" + (siteUser.ClientIdentityManagedExternalProvider ? "1" : "0");
                        }
                        hdnOIDToAllowPwd.Value = map;
                    }
                }
                //hide labels
                lblErrors.Visible = false;
                lblAddError.Visible = false;
                lblEditError.Visible = false;
               
                //if the page is not a postback, clear the viewstate and set the initial viewstate settings
                if (!Page.IsPostBack)
                {
                    //bind all organizations since only kgs super admin or kgs system admins have access to this page
                    BindOrganizations(ddlOrganization, ddlAddOrganization);

                    ViewState.Clear();
                    GridViewSortDirection = initialSortDirection;
                    GridViewSortExpression = initialSortExpression;

                    BindCountries(ddlAddCountry, ddlEditCountry);
                    BindCultures(ddlAddCulture, ddlEditCulture);

                    sessionState["Search"] = String.Empty;
                }

                //set global oid for client logged in as
                globalOID = DataMgr.OrganizationDataMapper.GetOIDByCID(siteUser.CID);

                UserDownloadArea.DropDownList = ddlOrganization;
                UserDownloadArea.IsKGSUserAdminPage = UserAuditDownloadArea.IsKGSUserAdminPage = true;
            }

        #endregion

        #region Set Fields and Data

            protected void SetUserIntoEditForm(User user, string primaryRoleID)
            {
                //Organization
                hdnEditOID.Value = user.OID.ToString();
                lblEditOrganizationName.Text = user.Organization.OrganizationName;

                //ID
                hdnEditUID.Value = Convert.ToString(user.UID);
                //First Name
                txtEditFirstName.Text = user.FirstName;
                //Last Name
                txtEditLastName.Text = user.LastName;
                //Email
                txtEmail.InnerText = user.Email;
                //Address
                txtEditAddress.Text = user.Address;
                //City
                txtEditCity.Text = user.City;
                //Country Alpha2Code
                ddlEditCountry.SelectedValue = user.CountryAlpha2Code;
                try
                {
                    ddlEditCountry_OnSelectedIndexChanged(null, null);

                    //StateID
                    try
                    {
                        ddlEditState.SelectedValue = Convert.ToString(user.StateID);
                    }
                    catch
                    {
                    }

                    //set zip validators accordingly
                    editZipRequiredValidator.Enabled = editZipRegExValidator.Enabled = user.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                    txtEditZip.Mask = user.CountryAlpha2Code == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa"; 
                }
                catch
                {
                }
                //Zip
                txtEditZip.Text = user.Zip;
                //Phone
                txtEditPhone.Text = user.Phone;
                //MobilePhone
                txtEditMobilePhone.Text = user.MobilePhone;
                //Culture
                ddlEditCulture.SelectedValue = Convert.ToString(user.LCID);

                //Bind Role based on organization
                BindPrimaryRoles(ddlEditRole, user.OID != 1, false, false, false);

                //primaryrole id
                ddlEditRole.SelectedValue = primaryRoleID;

                //set hdn edit role id to determine later if role was changed and what from and to
                hdnEditPrimaryRoleID.Value = primaryRoleID; 

                //enable role change
                chkEditRole.Checked = false;

                //role change disabled
                ddlEditRole.Enabled = false;

                //Organization Admin 
                chkEditOrganizationAdmin.Checked = user.IsOrganizationAdmin;
                chkEditOrganizationAdmin.Enabled = user.IsOrganizationAdmin;

                //Active
                chkEditActive.Checked = user.IsActive;

                //Test User
                chkEditTestUser.Checked = user.IsTestUser;

                //Test User
                chkEditLocked.Checked = user.IsLocked;

                //ResetPassword
                txtEditNewPassword.Text = "";

                //clear client listboxes, just in case
                ClearListbox(lbEditClientsTop);
                ClearListbox(lbEditClientsBottom);

                //Client Access depending on role.
                //show clients if pirimary role is restricte
                if (RoleHelper.IsRestricted(primaryRoleID)) 
                {
                    //bind clients for user
                    IEnumerable<Client> clients = RoleHelper.IsKGSFullAdminOrHigher(primaryRoleID) ? DataMgr.ClientDataMapper.GetAllActiveClients() : DataMgr.ClientDataMapper.GetAllActiveClientsAssociatedToUID(user.UID, RoleHelper.IsRestricted(primaryRoleID)); 

                    if (clients.Any())
                    {
                        //bind clients associated to user to bottom listbox
                        lbEditClientsBottom.DataSource = clients;
                        lbEditClientsBottom.DataTextField = "ClientName";
                        lbEditClientsBottom.DataValueField = "CID";
                        lbEditClientsBottom.DataBind();
                    }

                    //get clients not for user
                    IEnumerable<Client> clients2 = DataMgr.ClientDataMapper.GetAllClientsNotAssociatedToUserID(user.UID, RoleHelper.IsKGSRestrictedAdmin(primaryRoleID));

                    if (clients2.Any())
                    {
                        //bind clients not associated to user to top listbox                        
                        lbEditClientsTop.DataSource = clients2;
                        lbEditClientsTop.DataTextField = "ClientName";
                        lbEditClientsTop.DataValueField = "CID";
                        lbEditClientsTop.DataBind();
                    }

                    divEditClientsListBoxs.Visible = true;
                }
                else{
                    divEditClientsListBoxs.Visible = false;
                }
                ShowPwdArea(user.OID);
            }

            protected void SetUserClientRolesIntoEditForm(int uid, string primaryRoleID, string oid)
            {
                //set hidden oid
                hdnEditClientRolesOID.Value = oid;

                //set hidden primaryRoleID
                hdnEditClientRolesPrimaryRoleID.Value = primaryRoleID;

                //set hidden uid
                hdnEditClientRolesUID.Value = uid.ToString();

                //set clients
                BindClients(ddlEditClientRolesClient, primaryRoleID, uid.ToString(), null);

                //show panel
                pnlEditClientRoles.Visible = true;

                DisableClientRoleRestrictedControls(chkEditClientRolesRole, ddlEditClientRolesRole);
            }

            protected void SetUserClientModulesIntoEditForm(int uid)
            {
                //set clients
                BindClients(ddlEditClientModulesClient, null, uid.ToString(), null);

                //set hidden uid
                hdnEditClientModulesUID.Value = uid.ToString();

                //show panel
                pnlEditClientModules.Visible = true;

                //hide checkbox and listboxes
                pnlIsModuleRestricted.Visible = false;
                ToggleModuleRestrictedControls(false, divEditModulesListBoxs);
            }

            protected void SetUserClientQuickLinksIntoEditForm(int uid, string primaryRoleID)
            {
                //clear listboxes
                ClearListbox(lbEditClientQuickLinksTop);
                ClearListbox(lbEditClientQuickLinksBottom);

                //set hidden uid
                hdnEditClientQuickLinksUID.Value = uid.ToString();

                //set hidden primaryRoleID
                hdnEditClientQuickLinksPrimaryRoleID.Value = primaryRoleID;

                //set clients
                BindClients(ddlEditClientQuickLinksClient, primaryRoleID, uid.ToString(), null);

                //show panel
                pnlEditClientQuickLinks.Visible = true;
            }

            protected void SetUserClientBuildingsIntoEditForm(int uid, string oid)
            {
                hdnEditClientBuildingsOID.Value = oid;

                //bind buildings on client dropdown selection

                //set clients
                BindClients(ddlEditClientBuildingsClient, null, uid.ToString(), null);

                //set hidden uid
                hdnEditClientBuildingsUID.Value = uid.ToString();

                //show panel
                pnlEditClientBuildings.Visible = true;
                
                //hide checkbox and listboxes
                pnlIsBuildingRestricted.Visible = false;
                ToggleBuildingRestrictedControls(false, divEditBuildingGroupsListBoxs, divEditBuildingsListBoxs);
            }

            protected void SetUserClientBuildingModulesIntoEditForm(int uid)
            {
                //set hidden uid
                hdnEditClientBuildingModulesUID.Value = uid.ToString();

                //show panel
                pnlEditClientBuildingModules.Visible = true;
            }

        #endregion

        #region Load User

            protected void LoadAddFormIntoUser(User user)
            {
                //Organization
                user.OID = Convert.ToInt32(ddlAddOrganization.SelectedValue);
                //First Name
                user.FirstName = txtAddFirstName.Text;
                //Last Name
                user.LastName = txtAddLastName.Text;
                //Email
                user.Email = txtAddEmail.Text;
                //Address
                user.Address = txtAddAddress.Text;
                //City
                user.City = txtAddCity.Text;
                //CountryAlpha2Code
                user.CountryAlpha2Code = ddlAddCountry.SelectedValue;
                //StateID
                user.StateID = ddlAddState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlAddState.SelectedValue) : null;
                //ZIP
                user.Zip = String.IsNullOrEmpty(txtAddZip.Text) ? null : txtAddZip.Text; 
                //Phone
                user.Phone = !String.IsNullOrEmpty(txtAddPhone.Text) ? txtAddPhone.Text : null;                          
                //MobilePhone
                user.MobilePhone = !String.IsNullOrEmpty(txtAddMobilePhone.Text) ? txtAddMobilePhone.Text : null;
                //Culture
                user.LCID = Convert.ToInt32(ddlAddCulture.SelectedValue);

                //Primary Role
                //set role later on

                //Organization Admin
                user.IsOrganizationAdmin = chkAddOrganizationAdmin.Checked;

                user.IsActive = true;
                user.IsLocked = false;

                //Test User
                user.IsTestUser = chkAddTestUser.Checked;

                user.DateModified = DateTime.UtcNow;
            }

            protected void LoadEditFormIntoUser(User user)
            {
                //Organization
                //no need to edit organization

                //ID
                user.UID = Convert.ToInt32(hdnEditUID.Value);
                //First Name
                user.FirstName = txtEditFirstName.Text;
                //Last Name
                user.LastName = txtEditLastName.Text;
                //Email
                user.Email = txtEmail.InnerText;                
                //Address
                user.Address = txtEditAddress.Text;
                //City
                user.City = txtEditCity.Text;
                //CountryAlpha2Code
                user.CountryAlpha2Code = ddlEditCountry.SelectedValue;
                //StateID
                user.StateID = ddlEditState.SelectedValue != "-1" ? (int?)Convert.ToInt32(ddlEditState.SelectedValue) : null;
                //ZIP
                user.Zip = String.IsNullOrEmpty(txtEditZip.Text) ? null : txtEditZip.Text; 
                //Phone
                user.Phone = String.IsNullOrEmpty(txtEditPhone.Text) ? null : txtEditPhone.Text;
                //MobilePhone
                user.MobilePhone = String.IsNullOrEmpty(txtEditMobilePhone.Text) ? null : txtEditMobilePhone.Text;
                //Culture
                user.LCID = Convert.ToInt32(ddlEditCulture.SelectedValue);

                //Primary Role: Determine if role changed and what from and to later on

                //New Password
                user.Password = String.IsNullOrEmpty(txtEditNewPassword.Text) ? "" : Encryptor.getMd5Hash(txtEditNewPassword.Text);

                //Organization Admin 
                user.IsOrganizationAdmin = chkEditOrganizationAdmin.Checked;                

                //Active
                user.IsActive = chkEditActive.Checked;

                //Test User
                user.IsTestUser = chkEditTestUser.Checked;

                //Locked
                user.IsLocked = chkEditLocked.Checked;
            }

        #endregion
        
        #region Listbox Button Events

            /// <summary>
            /// on add Clients button up click, add clients
            /// </summary>
            protected void btnAddClientsUpButton_Click(object sender, EventArgs e)
            {
                while (lbAddClientsBottom.SelectedIndex != -1)
                {
                    lbAddClientsTop.Items.Add(lbAddClientsBottom.SelectedItem);
                    lbAddClientsBottom.Items.Remove(lbAddClientsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on add Clients button down click, add clients 
            /// </summary>
            protected void btnAddClientsDownButton_Click(object sender, EventArgs e)
            {
                while (lbAddClientsTop.SelectedIndex != -1)
                {
                    {
                        lbAddClientsBottom.Items.Add(lbAddClientsTop.SelectedItem);
                        lbAddClientsTop.Items.Remove(lbAddClientsTop.SelectedItem);
                    }
                }
            }

            /// <summary>
            /// on edit Clients button up click, add clients
            /// </summary>
            protected void btnEditClientsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientsBottom.SelectedIndex != -1)
                {
                    lbEditClientsTop.Items.Add(lbEditClientsBottom.SelectedItem);
                    lbEditClientsBottom.Items.Remove(lbEditClientsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit Clients button down click, add clients
            /// </summary>
            protected void btnEditClientsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientsTop.SelectedIndex != -1)
                {
                    lbEditClientsBottom.Items.Add(lbEditClientsTop.SelectedItem);
                    lbEditClientsTop.Items.Remove(lbEditClientsTop.SelectedItem);
                }
            }

            /// <summary>
            /// on edit BuildingGroups button up click, add building groups
            /// </summary>
            protected void btnEditClientBuildingGroupsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientBuildingGroupsBottom.SelectedIndex != -1)
                {
                    lbEditClientBuildingGroupsTop.Items.Add(lbEditClientBuildingGroupsBottom.SelectedItem);
                    lbEditClientBuildingGroupsBottom.Items.Remove(lbEditClientBuildingGroupsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit BuildingGroups button down click, add building groups
            /// </summary>
            protected void btnEditClientBuildingGroupsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientBuildingGroupsTop.SelectedIndex != -1)
                {
                    lbEditClientBuildingGroupsBottom.Items.Add(lbEditClientBuildingGroupsTop.SelectedItem);
                    lbEditClientBuildingGroupsTop.Items.Remove(lbEditClientBuildingGroupsTop.SelectedItem);
                }
            }

            /// <summary>
            /// on edit Buildings button up click, add buildings 
            /// </summary>
            protected void btnEditClientBuildingsUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientBuildingsBottom.SelectedIndex != -1)
                {
                    lbEditClientBuildingsTop.Items.Add(lbEditClientBuildingsBottom.SelectedItem);
                    lbEditClientBuildingsBottom.Items.Remove(lbEditClientBuildingsBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit Buildings button down click, add buildings 
            /// </summary>
            protected void btnEditClientBuildingsDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientBuildingsTop.SelectedIndex != -1)
                {
                    lbEditClientBuildingsBottom.Items.Add(lbEditClientBuildingsTop.SelectedItem);
                    lbEditClientBuildingsTop.Items.Remove(lbEditClientBuildingsTop.SelectedItem);
                }
            }

            /// <summary>
            /// on edit Modules button up click, add modules 
            /// </summary>
            protected void btnEditClientModulesUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientModulesBottom.SelectedIndex != -1)
                {
                    lbEditClientModulesTop.Items.Add(lbEditClientModulesBottom.SelectedItem);
                    lbEditClientModulesBottom.Items.Remove(lbEditClientModulesBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit Modules button down click, add modules 
            /// </summary>
            protected void btnEditClientModulesDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientModulesTop.SelectedIndex != -1)
                {
                    lbEditClientModulesBottom.Items.Add(lbEditClientModulesTop.SelectedItem);
                    lbEditClientModulesTop.Items.Remove(lbEditClientModulesTop.SelectedItem);
                }
            }

            /// <summary>
            /// on edit QuickLinks button up click, add QuickLinks
            /// </summary>
            protected void btnEditClientQuickLinksUpButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientQuickLinksBottom.SelectedIndex != -1)
                {
                    lbEditClientQuickLinksTop.Items.Add(lbEditClientQuickLinksBottom.SelectedItem);
                    lbEditClientQuickLinksBottom.Items.Remove(lbEditClientQuickLinksBottom.SelectedItem);
                }
            }

            /// <summary>
            /// on edit QuickLinks button down click, add QuickLinks
            /// </summary>
            protected void btnEditClientQuickLinksDownButton_Click(object sender, EventArgs e)
            {
                while (lbEditClientQuickLinksTop.SelectedIndex != -1)
                {
                    lbEditClientQuickLinksBottom.Items.Add(lbEditClientQuickLinksTop.SelectedItem);
                    lbEditClientQuickLinksTop.Items.Remove(lbEditClientQuickLinksTop.SelectedItem);
                }
            }
        
        #endregion

        #region Dropdown Events

            protected void ddlOrganization_OnSelectedIndexChanged(Object sender, EventArgs e)
            {
                var isOrgSelected = ddlOrganization.SelectedValue != "-1";

                ControlHelper.ToggleVisibility(isOrgSelected, new Control[] { UserDownloadArea });

                UserDownloadArea.FindControl("lblError").Visible = false;
                dtvUser.Visible = false; // Hide User details; will be made visible when a particular user is selected off grid

                if (isOrgSelected)
                {
                    txtSearch.Text = sessionState["Search"] = String.Empty;
                    pnlEditUsers.Visible = false;
                    BindUsers();
                    return;
                }
                    
                pnlSearch.Visible = false;
            }

            protected void ddlAddOrganization_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                string selectedRoleValue = ddlAddRole.SelectedValue;
                string selectedOrganizationValue = ddlAddOrganization.SelectedValue;

                if (selectedOrganizationValue != "-1")
                {
                    //if organization is kgs rebind roles with kgs roles, else without
                    BindPrimaryRoles(ddlAddRole, selectedOrganizationValue != "1", false, false, false);

                    //clear selected value
                    ddlAddRole.SelectedValue = "-1";

                    //clear listboxes
                    ClearListbox(lbAddClientsTop);
                    ClearListbox(lbAddClientsBottom);
                    ShowPwdArea(Convert.ToInt32(selectedOrganizationValue));
                }
            }

            protected void ddlEditRole_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                string selectedRoleValue = ddlEditRole.SelectedValue;

                //based on role enable or disable and clear orgadmin checkbox. only suuper admins or kgs system admins can be an orgadmin
                if (RoleHelper.IsKGSSuperAdminOrHigher(selectedRoleValue) || RoleHelper.IsSuperAdmin(selectedRoleValue))
                {
                    chkEditOrganizationAdmin.Enabled = true;
                }
                else
                {
                    chkEditOrganizationAdmin.Checked = false;
                    chkEditOrganizationAdmin.Enabled = false;
                }

                //show all available clients if restricted
                if (RoleHelper.IsRestricted(selectedRoleValue))
                {
                    divEditClientsListBoxs.Visible = true;

                    //clear listboxes
                    ClearListbox(lbEditClientsTop);
                    ClearListbox(lbEditClientsBottom);

                    //rebind listboxes again to listboxs on edit user page
                    //get clients not associated to userid but available
                    IEnumerable<Client> clients = DataMgr.ClientDataMapper.GetAllClientsNotAssociatedToUserID(Convert.ToInt32(hdnEditUID.Value), RoleHelper.IsKGSRestrictedAdmin(selectedRoleValue));

                    //bind clients to top listbox
                    lbEditClientsTop.DataSource = clients.OrderBy(c => c.ClientName);
                    lbEditClientsTop.DataTextField = "ClientName";
                    lbEditClientsTop.DataValueField = "CID";
                    lbEditClientsTop.DataBind();


                    //get all clients already assigned and avaialable by client id
                    IEnumerable<Client> clients2 = DataMgr.ClientDataMapper.GetAllClientsAssignedToUserAndAvailable(DataMgr.ClientDataMapper.GetAllActiveClients(), (Convert.ToInt32(hdnEditUID.Value)));

                    //bind clients to bottom listbox
                    lbEditClientsBottom.DataSource = clients2.OrderBy(c => c.ClientName);
                    lbEditClientsBottom.DataTextField = "ClientName";
                    lbEditClientsBottom.DataValueField = "CID";
                    lbEditClientsBottom.DataBind();
                }
                else
                {
                    //clear listboxes
                    ClearListbox(lbEditClientsTop);
                    ClearListbox(lbEditClientsBottom);

                    divEditClientsListBoxs.Visible = false;
                }
            }

            protected void ddlAddRole_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                string selectRoleValue = ddlAddRole.SelectedValue;
                string selectOrganizationValue = ddlAddOrganization.SelectedValue;

                //if role selected
                if (selectRoleValue != "-1")
                {
                    //if organization selected
                    if (selectOrganizationValue != "-1")
                    {
                        RearrangeAddForm(selectOrganizationValue, selectRoleValue);
                    }
                }
            }

            protected void ddlEditClientRolesClient_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                string primaryRoleID = hdnEditClientRolesPrimaryRoleID.Value;
                int selectedCID = Convert.ToInt32(ddlEditClientRolesClient.SelectedValue);
                int? maxRoleFilter = null;

                //get userclient 
                var userClientFirst = DataMgr.UserClientDataMapper.GetUsersClientsFirst(Convert.ToInt32(hdnEditClientRolesUID.Value), selectedCID);

                //get providers if any, then max allowed role for that client based on all providers.
                var providerIDs = DataMgr.ProviderDataMapper.GetAllProvidersIDsByOrg(Convert.ToInt32(hdnEditClientRolesOID.Value));
                ProvidersClients pc = null;

                if(providerIDs.Any())
                {
                    var providerclients = DataMgr.ProviderClientDataMapper.GetProvidersClientsListByProviderIDs(providerIDs);

                    //get if any for that client
                    pc = providerclients.Where(pcs => pcs.CID == selectedCID).OrderBy(pcs => pcs.MaxRoleID).FirstOrDefault();

                    if (pc != null)
                        maxRoleFilter = pc.MaxRoleID;
                }
               
                //bind roles, but show and set later after client selected.
                //keep existing filters as maxrole could be higher than the type of admin page, so we still want to filter out kgs.
                BindSecondaryRoles(ddlEditClientRolesRole, !RoleHelper.IsKGSRestrictedAdmin(primaryRoleID), !RoleHelper.IsRestrictedAdmin(primaryRoleID), maxRoleFilter); 

                //set current selected value.
                string secondaryRoleID = userClientFirst.SecondaryRoleID.ToString();

                try
                {
                    ddlEditClientRolesRole.SelectedValue = secondaryRoleID;
                }
                catch
                {
                    LogMgr.Log(DataConstants.LogLevel.INFO, String.Format("Provider client user has a secondary client restricted role they should not have based on maxrole: UID={0}, OID={1}, ProviderID={2}, CID={3}.", hdnEditClientRolesUID.Value, hdnEditClientRolesOID.Value, pc != null ? pc.ProviderID.ToString() : "", selectedCID));
                }

                hdnEditClientRolesSecondaryRoleID.Value = secondaryRoleID;
            }

            //no need for ddlEditClientRolesRole_OnSelectedIndexChanged

            protected void ddlEditClientModulesClient_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlEditClientModulesClient.SelectedIndex == 0) { return; }

                int cid = Convert.ToInt32(ddlEditClientModulesClient.SelectedValue);                
                int uid = Convert.ToInt32(hdnEditClientModulesUID.Value);

                UsersClients userClient = DataMgr.UserClientDataMapper.GetUsersClientsFirst(uid, cid);

                //set primary roleid for inserting new records later 
                hdnEditClientModulesPrimaryRoleID.Value = userClient.PrimaryRoleID.ToString();

                //set secondary roleid for inserting new records later 
                hdnEditClientModulesSecondaryRoleID.Value = userClient.SecondaryRoleID.ToString();

                BindModules(cid, lbEditClientModulesTop, lbEditClientModulesBottom, hdnEditClientModulesUID, chkIsModuleRestricted);

                //if bottom list boxes have anything assigned, then set restricted as checked
                if (lbEditClientModulesBottom.Items.Count > 0)
                {
                    chkIsModuleRestricted.Checked = true;
                    chkIsModuleRestricted.Enabled = false;
                    ToggleModuleRestrictedControls(true, divEditModulesListBoxs);
                }
                else
                {
                    chkIsModuleRestricted.Checked = false;
                    chkIsModuleRestricted.Enabled = true;
                    ToggleModuleRestrictedControls(false, divEditModulesListBoxs);
                }

                //if userClientFirst has moduleid = 0, then they are unrestrictedmodules and set hdnUnrestricted as true
                if (userClient.ModuleID == 0)
                {
                    hdnEditClientModulesUnrestricted.Value = "true";
                }

                pnlIsModuleRestricted.Visible = true;
            }

            protected void ddlEditClientQuickLinksClient_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                //BindInitialSetEditUserQuickLinks();   

                //BindQuickLinks(false, true, false, true);

                if (ddlEditClientQuickLinksClient.SelectedValue != "-1")
                {
                    int hdnUID = Convert.ToInt32(hdnEditClientQuickLinksUID.Value);
                    int selectedCID = Convert.ToInt32(ddlEditClientQuickLinksClient.SelectedValue);

                    //if primary role restricted get secondary role for client user.
                    int roleID = RoleHelper.IsRestricted(hdnEditClientQuickLinksPrimaryRoleID.Value) ? (int)DataMgr.UserClientDataMapper.GetUsersClientsSecondaryRoleID(hdnUID, selectedCID) : Convert.ToInt32(hdnEditClientQuickLinksPrimaryRoleID.Value);

                    //bind based on primary or secondary roleid
                    BindUserClientQuickLinks(hdnUID, selectedCID, roleID);
                }
            }

            protected void ddlEditClientBuildingsClient_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                if (ddlEditClientBuildingsClient.SelectedIndex == 0) { return; }

                int cid = Convert.ToInt32(ddlEditClientBuildingsClient.SelectedValue);
                int oid = Convert.ToInt32(hdnEditClientBuildingsOID.Value);
                int uid = Convert.ToInt32(hdnEditClientBuildingsUID.Value);

                UsersClients userClient = DataMgr.UserClientDataMapper.GetUsersClientsFirst(uid, cid);
                
                //set primary roleid for inserting new records later 
                hdnEditClientBuildingsPrimaryRoleID.Value = userClient.PrimaryRoleID.ToString();

                //set secondary roleid for inserting new records later 
                hdnEditClientBuildingsSecondaryRoleID.Value = userClient.SecondaryRoleID.ToString();

                BindBuildingGroups(cid, oid, lbEditClientBuildingGroupsTop, lbEditClientBuildingGroupsBottom, hdnEditClientBuildingsUID, chkIsBuildingRestricted);
                BindBuildings(cid, oid, lbEditClientBuildingsTop, lbEditClientBuildingsBottom, hdnEditClientBuildingsUID, chkIsBuildingRestricted);

                //if bottom list boxes have anything assigned, then set restricted as checked
                if (lbEditClientBuildingsBottom.Items.Count > 0 || lbEditClientBuildingGroupsBottom.Items.Count > 0)
                {
                    chkIsBuildingRestricted.Checked = true;
                    chkIsBuildingRestricted.Enabled = false;
                    ToggleBuildingRestrictedControls(true, divEditBuildingGroupsListBoxs, divEditBuildingsListBoxs);
                }
                else
                {
                    chkIsBuildingRestricted.Checked = false;
                    chkIsBuildingRestricted.Enabled = true;
                    ToggleBuildingRestrictedControls(false, divEditBuildingGroupsListBoxs, divEditBuildingsListBoxs);
                }

                //if userClient.First has bid = 0 and bgid = 0, then they are unrestrictedbuildings and set hdnUnrestricted true
                if (userClient.BID == 0 && userClient.BuildingGroupID == 0)
                {
                    hdnEditClientBuildingsUnrestricted.Value = "true";
                }

                pnlIsBuildingRestricted.Visible = true;
            }

            protected void ddlEditClientBuildingModulesClient_OnSelectedIndexChanged(object sender, EventArgs e)
            {
            }

            protected void ddlAddCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlAddState, ddlAddCountry.SelectedValue);

                addZipRequiredValidator.Enabled = addZipRegExValidator.Enabled = ddlAddCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtAddZip.Mask = ddlAddCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
                addStateRequiredValidator.Enabled = ddlAddState.Items.Count > 1;
            }

            protected void ddlEditCountry_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                BindStates(ddlEditState, ddlEditCountry.SelectedValue);

                editZipRequiredValidator.Enabled = editZipRegExValidator.Enabled = ddlEditCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? true : false;
                txtEditZip.Mask = ddlEditCountry.SelectedValue == BusinessConstants.Countries.UnitedStatesAlpha2Code ? "#####" : "aaaaaaaaaa";
                editStateRequiredValidator.Enabled = ddlEditState.Items.Count > 1;
            }

        #endregion

        #region Checkbox Events

            protected void chkEditRole_OnCheckedChanged(object sender, EventArgs e)
            {
                ddlEditRole.Enabled = chkEditRole.Checked;

                //no need to clear list boxes and the ddl selected index change will
            }

            protected void chkEditClientRolesRole_OnCheckedChanged(object sender, EventArgs e)
            {
                ddlEditClientRolesRole.Enabled = chkEditClientRolesRole.Checked;
            }

            protected void chkIsBuildingRestricted_OnCheckedChanged(object sender, EventArgs e)
            {
                ToggleBuildingRestrictedControls(chkIsBuildingRestricted.Checked, divEditBuildingGroupsListBoxs, divEditBuildingsListBoxs);
            }
            protected void chkIsModuleRestricted_OnCheckedChanged(object sender, EventArgs e)
            {
                ToggleModuleRestrictedControls(chkIsModuleRestricted.Checked, divEditModulesListBoxs);
            }

        #endregion

        #region Button Events

            /// <summary>
            /// Add user Button on click.
            /// </summary>
            protected void addUserButton_Click(object sender, EventArgs e)
            {
                //check that user does not already exist
                if (!DataMgr.UserDataMapper.DoesUserExist(txtAddEmail.Text))
                {
                    int newUserID = 0;
                    bool userInsertSuccess = true;

                    mUser = new User();

                    //load the form into the user
                    LoadAddFormIntoUser(mUser);

                    //get primary role
                    string primaryRoleID = ddlAddRole.SelectedValue;

                    //check if any form of restricted, else its a full adin or user
                    bool isAnyFormOfRestricted = RoleHelper.IsRestricted(primaryRoleID);
                                                          
                    //if client restricted
                    if (isAnyFormOfRestricted)
                    {
                        //we cant add a user thats client restricted without at least one client being added
                        //assign user client if appropriate role and/or clients exist in listbox---
                        if (lbAddClientsBottom.Items.Count > 0)
                        {
                            bool isPublicOrKiosk = RoleHelper.IsPublicOrKioskUser(primaryRoleID);

                            //check if public or kiosk user already exists. can only have one of each per client.
                            if (isPublicOrKiosk && lbAddClientsBottom.Items.Count > 1)
                            {
                                userInsertSuccess = false;
                                LabelHelper.SetLabelMessage(lblAddError, publicOrKioskClientRestriction, lnkSetFocusAdd);
                            }
                            else if (isPublicOrKiosk && DataMgr.UserDataMapper.GetUsersClientsUsersByRole(Convert.ToInt32(lbAddClientsBottom.Items[0].Value), Convert.ToInt32(primaryRoleID)).Any())
                            {
                                userInsertSuccess = false;
                                LabelHelper.SetLabelMessage(lblAddError, publicOrKioskUserExists, lnkSetFocusAdd);
                            }
                            else
                            {
                                //try to email the new user
                                userInsertSuccess = EmailUser();

                                if(userInsertSuccess)
                                {                                 
                                    //try to insert the new user
                                    try
                                    {
                                        //insert new user
                                        newUserID = DataMgr.UserDataMapper.InsertAndReturnUID(mUser);
                                        LabelHelper.SetLabelMessage(lblAddError, mUser.Email + addUserSuccess, lnkSetFocusAdd);
                                    }
                                    catch (SqlException sqlEx)
                                    {
                                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding user in kgs user administration.", sqlEx);
                                        LabelHelper.SetLabelMessage(lblAddError, addUserFailed, lnkSetFocusAdd);
                                        userInsertSuccess = false;
                                    }
                                    catch (Exception ex)
                                    {
                                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding user in kgs user administration.", ex);
                                        LabelHelper.SetLabelMessage(lblAddError, addUserFailed, lnkSetFocusAdd);
                                        userInsertSuccess = false;
                                    }
                                }


                                //declare new usersclients azure table model
                                UsersClients mUsersClient = new UsersClients();

                                if (userInsertSuccess)
                                {
                                    //try to insert the client to user
                                    try
                                    {
                                        //inserting each client in the bottom listbox.
                                        //no need to check if they exist already since its the first addition                                
                                        foreach (ListItem item in lbAddClientsBottom.Items)
                                        {
                                            mUsersClient.CID = Convert.ToInt32(item.Value);
                                            mUsersClient.UID = newUserID;
                                            mUsersClient.PrimaryRoleID = Convert.ToInt32(primaryRoleID);
                                            mUsersClient.BuildingGroupID = 0;
                                            mUsersClient.BID = 0;
                                            mUsersClient.DateCreated = DateTime.UtcNow;
                                            mUsersClient.DateModified = DateTime.UtcNow;
                                            mUsersClient.Guid = new Guid();

                                            //set module to kiosk if initially kiosk role, else 0 for all
                                            mUsersClient.ModuleID = RoleHelper.IsKioskUser(primaryRoleID) ? Convert.ToInt32(BusinessConstants.Module.Modules.Kiosk) : 0;

                                            //set hardcoded role based on restricted level
                                            //kgs full admin - 200
                                            //full admin - 500
                                            //full user - 700
                                            //public user - 900
                                            //kiosk user - 1000
                                            mUsersClient.SecondaryRoleID = RoleHelper.IsKGSRestrictedAdmin(primaryRoleID) ? 200 : (RoleHelper.IsRestrictedAdmin(primaryRoleID) ? 500 : (RoleHelper.IsRestrictedUser(primaryRoleID) ? 700 : (RoleHelper.IsPublicUser(primaryRoleID) ? 900 : 1000)));

                                            //UserDataMapper.InsertUserClient(mUsersClient);
                                            DataMgr.UserClientDataMapper.AddUsersClients(mUsersClient);
                                        }
                                    }
                                    catch (SqlException sqlEx)
                                    {
                                        //roll back user
                                        DataMgr.UserDataMapper.RollBackUser(newUserID);

                                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning clients to restricted client user in kgs user administration.", sqlEx);
                                        LabelHelper.SetLabelMessage(lblAddError, assigningClientsFailed, lnkSetFocusAdd);
                                        userInsertSuccess = false;
                                    }
                                    catch (Exception ex)
                                    {
                                        //roll back user
                                        DataMgr.UserDataMapper.RollBackUser(newUserID);

                                        LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning clients to restricted client user in kgs user administration.", ex);
                                        LabelHelper.SetLabelMessage(lblAddError, assigningClientsFailed, lnkSetFocusAdd);
                                        userInsertSuccess = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            LabelHelper.SetLabelMessage(lblAddError, clientRequired, lnkSetFocusAdd);
                            userInsertSuccess = false;
                        }
                    }                  
                    else
                    {
                        int primaryCID = DataMgr.ClientDataMapper.GetPrimaryCIDByOID(mUser.OID);

                        if (primaryCID != 0)
                        {
                            //try to email the new user
                            userInsertSuccess = EmailUser();

                            if(userInsertSuccess)
                            {
                                //try to insert the new user
                                try
                                {
                                    //insert new user
                                    newUserID = DataMgr.UserDataMapper.InsertAndReturnUID(mUser);

                                    LabelHelper.SetLabelMessage(lblAddError, mUser.Email + addUserSuccess, lnkSetFocusAdd);
                                }
                                catch (SqlException sqlEx)
                                {
                                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding user in kgs user administration.", sqlEx);
                                    LabelHelper.SetLabelMessage(lblAddError, addUserFailed, lnkSetFocusAdd);
                                    userInsertSuccess = false;
                                }
                                catch (Exception ex)
                                {
                                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding user in kgs user administration.", ex);
                                    LabelHelper.SetLabelMessage(lblAddError, addUserFailed, lnkSetFocusAdd);
                                    userInsertSuccess = false;
                                }
                            }

                            //declare new usersclients azure table model
                            UsersClients mUsersClient = new UsersClients();

                            if (userInsertSuccess)
                            {
                                //try to insert the client to user
                                try
                                {
                                    //inserting new user client association to primary client

                                    mUsersClient.CID = primaryCID;
                                    mUsersClient.UID = newUserID;
                                    mUsersClient.PrimaryRoleID = Convert.ToInt32(primaryRoleID);
                                    mUsersClient.BuildingGroupID = 0;
                                    mUsersClient.BID = 0;
                                    mUsersClient.DateCreated = DateTime.UtcNow;
                                    mUsersClient.DateModified = DateTime.UtcNow;
                                    mUsersClient.Guid = new Guid();
                                    mUsersClient.ModuleID = 0;

                                    mUsersClient.SecondaryRoleID = null;

                                    DataMgr.UserClientDataMapper.AddUsersClients(mUsersClient);
                                }
                                catch (SqlException sqlEx)
                                {
                                    //roll back user
                                    DataMgr.UserDataMapper.RollBackUser(newUserID);

                                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning primary user client association to user in kgs user administration.", sqlEx);
                                    LabelHelper.SetLabelMessage(lblAddError, assigningClientsFailed, lnkSetFocusAdd); 
                                    userInsertSuccess = false;
                                }
                                catch (Exception ex)
                                {
                                    //roll back user
                                    DataMgr.UserDataMapper.RollBackUser(newUserID);

                                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error assigning primary user client association to user in kgs user administration.", ex);
                                    LabelHelper.SetLabelMessage(lblAddError, assigningClientsFailed, lnkSetFocusAdd); 
                                    userInsertSuccess = false;
                                }
                            }
                        }
                        else
                        {
                            userInsertSuccess = false;
                            LabelHelper.SetLabelMessage(lblAddError, primaryClientMissing, lnkSetFocusAdd);
                        }
                    }

                    //add userAudit table entry
                    if (userInsertSuccess)
                    {
                        //declare new userAudit
                        UsersAudit mUserAudit = new UsersAudit();
                        mUserAudit.UID = newUserID;
                        mUserAudit.DateModified = DateTime.UtcNow;

                        //try to insert the audit for user
                        try
                        {
                            DataMgr.UserAuditDataMapper.InsertUserAudit(mUserAudit);                          
                        }
                        catch (SqlException sqlEx)
                        {
                            //roll back user
                            DataMgr.UserDataMapper.RollBackUser(newUserID);

                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding userAudit for user in kgs user administration.", sqlEx);
                            LabelHelper.SetLabelMessage(lblAddError, addingAuditForUserFailed, lnkSetFocusAdd); 
                        }
                        catch (Exception ex)
                        {
                            //roll back user
                            DataMgr.UserDataMapper.RollBackUser(newUserID);

                            LogMgr.Log(DataConstants.LogLevel.ERROR, "Error adding userAudit for user in kgs user administration.", ex);
                            LabelHelper.SetLabelMessage(lblAddError, addingAuditForUserFailed, lnkSetFocusAdd);
                        }
                    }
                }
                else
                {
                    //user exists
                    LabelHelper.SetLabelMessage(lblAddError, userExists, lnkSetFocusAdd);
                }   
            }

            /// <summary>
            /// Update user Button on click.
            /// </summary>
            protected void updateUserButton_Click(object sender, EventArgs e)
            {
                //create and load the form into the user
                mUser = new User();
                LoadEditFormIntoUser(mUser);

                //check if the user email exists for another user if trying to change
                if (DataMgr.UserDataMapper.DoesUserExist(mUser.UID, mUser.Email))
                {
                    LabelHelper.SetLabelMessage(lblEditError, userExists, lnkEditSetFocusView);
                    return;
                }
                //update the user
                else
                {
                    string selectedPrimaryRoleID = ddlEditRole.SelectedValue;
                    bool isAnyFormOfRestricted = RoleHelper.IsRestricted(ddlEditRole.SelectedValue);

                    //check if client restriected first
                    if (isAnyFormOfRestricted)
                    {
                        //before determining if a role changed check if any clients are in the listbox
                        //we cant update a user thats client restricted without at least one client assigned
                        if (lbEditClientsBottom.Items.Count > 0)
                        {
                            //if public or kiosk
                            bool isPublicOrKiosk = RoleHelper.IsPublicOrKioskUser(selectedPrimaryRoleID);

                            //IF ROLE CHANGED--
                            if (selectedPrimaryRoleID != "-1" && hdnEditPrimaryRoleID.Value != selectedPrimaryRoleID)
                            {
                                //check if public or kiosk user already exists. can only have one of each per client.
                                if (isPublicOrKiosk && lbEditClientsBottom.Items.Count > 1)
                                {
                                    LabelHelper.SetLabelMessage(lblEditError, publicOrKioskClientRestriction, lnkEditSetFocusView);
                                    return;
                                }
                                else if (isPublicOrKiosk && DataMgr.UserDataMapper.GetUsersClientsUsersByRole(Convert.ToInt32(lbEditClientsBottom.Items[0].Value), Convert.ToInt32(selectedPrimaryRoleID)).Any())
                                {
                                    LabelHelper.SetLabelMessage(lblEditError, publicOrKioskClientRestriction, lnkEditSetFocusView);
                                    return;
                                }
                                else
                                {
                                    //if role changed we may have to clean up/delete table records
                                    CleanUpTablesByRoleChange(mUser.UID, null, selectedPrimaryRoleID, true);
                                    AnyAuthzChanges = true;
                                }
                            }
                            else
                            {
                                //check if public or kiosk user already exists. can only have one of each per client.
                                if (isPublicOrKiosk && lbEditClientsBottom.Items.Count > 1)
                                {
                                    LabelHelper.SetLabelMessage(lblEditError, publicOrKioskClientRestriction, lnkEditSetFocusView);
                                    return;
                                }
                            }


                            //Client listbox changes-----
                            //we have to clear data for clients that are removed        
                            try
                            {
                                //declare new usersclients azure table model
                                UsersClients mUsersClient = new UsersClients();

                                //get clients for user
                                IEnumerable<Client> currentAssociatedClients = RoleHelper.IsKGSFullAdminOrHigher(selectedPrimaryRoleID) ? DataMgr.ClientDataMapper.GetAllActiveClients() : DataMgr.ClientDataMapper.GetAllActiveClientsAssociatedToUID(mUser.UID, true);

                                //get clients not for user
                                //IEnumerable<Client> currentUnassocaitedClients = ClientDataMapper.GetAllClientsNotAssociatedToUserID(mUser.UID, RoleHelper.IsKGSRestrictedAdmin(selectedPrimaryRole));

                                //insert each client in the bottom listbox if they dont exist
                                foreach (ListItem item in lbEditClientsBottom.Items)
                                {
                                    int cid = Convert.ToInt32(item.Value);

                                    if (!currentAssociatedClients.Any(c => c.CID == cid))
                                    {
                                        //insert new
                                        mUsersClient.CID = cid;
                                        mUsersClient.UID = mUser.UID;
                                        mUsersClient.PrimaryRoleID = Convert.ToInt32(selectedPrimaryRoleID);
                                        mUsersClient.BuildingGroupID = 0;
                                        mUsersClient.BID = 0;
                                        mUsersClient.DateCreated = DateTime.UtcNow;
                                        mUsersClient.DateModified = DateTime.UtcNow;
                                        mUsersClient.Guid = new Guid();
                                        mUsersClient.ModuleID = RoleHelper.IsKioskUser(selectedPrimaryRoleID) ? Convert.ToInt32(BusinessConstants.Module.Modules.Kiosk) : 0;

                                        //set hardcoded role based on restricted level
                                        //kgs full admin - 200
                                        //full admin - 500
                                        //full user - 700
                                        //public user - 900
                                        //kiosk user - 1000
                                        mUsersClient.SecondaryRoleID = RoleHelper.IsKGSRestrictedAdmin(selectedPrimaryRoleID) ? 200 : (RoleHelper.IsRestrictedAdmin(selectedPrimaryRoleID) ? 500 : (RoleHelper.IsRestrictedUser(selectedPrimaryRoleID) ? 700 : (RoleHelper.IsPublicUser(selectedPrimaryRoleID) ? 900 : 1000)));

                                        DataMgr.UserClientDataMapper.AddUsersClients(mUsersClient);
                                    }
                                }

                                if (mUsersClient.CID > 0)
                                {
                                    AnyAuthzChanges = true;
                                }

                                //deleting each client in the top listbox if they exist
                                foreach (ListItem item in lbEditClientsTop.Items)
                                {
                                    int cid = Convert.ToInt32(item.Value);

                                    //if the client exists, delete
                                    if (currentAssociatedClients.Any(c => c.CID == cid))
                                    {
                                        // delete user groups user relationships associated to user and client
                                        //mDataManager.UserGroupDataMapper.DeleteUserGroupUserOnClientRemoval(mUser.UID, cid);

                                        //delete other related assocaitions

                                        //deletes quicklinks for client user
                                        DataMgr.QuickLinksDataMapper.DeleteAllUserClientQuickLinksByCIDAndUID(cid, mUser.UID);

                                        //delete client user analysis builder views
                                        DataMgr.AnalysisBuilderDataMapper.DeleteViewsByCIDAndUID(cid, mUser.UID);

                                        //delete from azure
                                        DataMgr.UserClientDataMapper.DeleteUsersClients(DataMgr.UserClientDataMapper.GetUsersClients(mUser.UID, cid).ToList());
                                        AnyAuthzChanges = true;
                                    }
                                }
                            }
                            catch (SqlException sqlEx)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning clients to restricted client user in kgs user administration.", sqlEx);
                                LabelHelper.SetLabelMessage(lblEditError, reassigningClientsFailed, lnkEditSetFocusView);
                                return;
                            }
                            catch (Exception ex)
                            {
                                LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning clients to restricted client user in kgs user administration.", ex);
                                LabelHelper.SetLabelMessage(lblEditError, reassigningClientsFailed, lnkEditSetFocusView);
                                return;
                            }
                        }
                        else
                        {
                            LabelHelper.SetLabelMessage(lblEditError, clientRequired, lnkEditSetFocusView);
                            return;
                        }
                    }
                    else
                    {
                        //IF ROLE CHANGED--
                        if (selectedPrimaryRoleID != "-1" && hdnEditPrimaryRoleID.Value != selectedPrimaryRoleID)
                        {
                            //if role changed we may have to clean up/delete table records
                            CleanUpTablesByRoleChange(mUser.UID, null, selectedPrimaryRoleID, true);

                            //declare new usersclients azure table model
                            UsersClients mUsersClient = new UsersClients();

                            //create new userclient record for primary client
                            //insert new
                            mUsersClient.CID = DataMgr.ClientDataMapper.GetPrimaryCIDByOID(Convert.ToInt32(hdnEditOID.Value));
                            mUsersClient.UID = mUser.UID;
                            mUsersClient.PrimaryRoleID = Convert.ToInt32(selectedPrimaryRoleID);
                            mUsersClient.BuildingGroupID = 0;
                            mUsersClient.BID = 0;
                            mUsersClient.DateCreated = DateTime.UtcNow;
                            mUsersClient.DateModified = DateTime.UtcNow;
                            mUsersClient.Guid = new Guid();
                            mUsersClient.ModuleID = 0;

                            //secondary role is null since not restricted
                            mUsersClient.SecondaryRoleID = null;

                            DataMgr.UserClientDataMapper.AddUsersClients(mUsersClient);
                            AnyAuthzChanges = true;
                        }
                    }


                    try
                    {
                        //if new password was entered
                        if (!String.IsNullOrEmpty(txtEditNewPassword.Text))
                        {
                            string[] emailSplit = mUser.Email.Substring(0, mUser.Email.IndexOf("@")).Split('.');

                            foreach (string es in emailSplit)
                            {
                                if (es.Length >= 3 && txtEditNewPassword.Text.ToLower().Contains(es.ToLower()))
                                {
                                    LabelHelper.SetLabelMessage(lblEditError, invalidPasswordEmail, lnkEditSetFocusView);
                                    return;
                                }
                            }

                            if ((txtEditFirstName.Text.Length >= 3 && txtEditNewPassword.Text.ToLower().Contains(txtEditFirstName.Text.ToLower())) || (txtEditLastName.Text.Length >= 3 && txtEditNewPassword.Text.ToLower().Contains(txtEditLastName.Text.ToLower())))
                            {
                                LabelHelper.SetLabelMessage(lblEditError, invalidPasswordFirstOrLast, lnkEditSetFocusView);
                                return;
                            }

                            DataMgr.UserDataMapper.UpdateUser(mUser, true, true, false);
                            LabelHelper.SetLabelMessage(lblEditError, mUser.Email + updateSuccessful, lnkEditSetFocusView);
                        }
                        else
                        {
                            DataMgr.UserDataMapper.UpdateUser(mUser, false, true, false);
                            LabelHelper.SetLabelMessage(lblEditError, mUser.Email + updateSuccessful, lnkEditSetFocusView);
                        }

                        //update hidden variable to new values for multiple updates in a row
                        hdnEditPrimaryRoleID.Value = selectedPrimaryRoleID;
                    }
                    catch (Exception ex)
                    {
                        LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error updating user in kgs user administration: UserId={0}, UserEmail={1}, UserFirstName={2} UserLastName={3}.", mUser.UID, mUser.Email, mUser.FirstName, mUser.LastName), ex);
                        LabelHelper.SetLabelMessage(lblEditError, updateFailed, lnkEditSetFocusView);
                    }

                    //Bind users again
                    BindUsers();
                }

                editConfirmNewPasswordRequiredValidator.IsValid = true;
            }

            /// <summary>
            /// Update client roles Button on click.
            /// </summary>
            protected void updateClientRolesButton_Click(object sender, EventArgs e)
            {
                //get hidden uid
                int hiddenUID = Convert.ToInt32(hdnEditClientRolesUID.Value);
                string hiddenSecondaryRoleID = hdnEditClientRolesSecondaryRoleID.Value;    
                string selectedSecondaryRoleID = ddlEditClientRolesRole.SelectedValue;
                string selectedCID = ddlEditClientRolesClient.SelectedValue;

                try
                {
                    //if role changed
                    if (hiddenSecondaryRoleID != selectedSecondaryRoleID)
                    {
                        //cleanup tables
                        CleanUpTablesByRoleChange(hiddenUID, Convert.ToInt32(selectedCID), hiddenSecondaryRoleID, false);

                        //get all userclients records for uid and cid
                        var usersClients = DataMgr.UserClientDataMapper.GetUsersClients(hiddenUID, Convert.ToInt32(selectedCID));

                        //foreach usersclients update role
                        foreach (UsersClients uc in usersClients)
                        {
                            uc.SecondaryRoleID = Convert.ToInt32(selectedSecondaryRoleID);
                            uc.DateModified = DateTime.UtcNow;
                            DataMgr.UserClientDataMapper.UpdateUsersClients(uc);
                            //UserDataMapper.UpdateUserClientByCIDAndUID(Convert.ToInt32(ddlEditClientRolesClient.SelectedValue), hiddenUID, Convert.ToInt32(hiddenSecondaryRoleID));
                        }

                        //update hidden variable to new values for multiple updates in a row
                        hdnEditClientRolesSecondaryRoleID.Value = selectedSecondaryRoleID;

                        LabelHelper.SetLabelMessage(lblEditClientRolesError, updateRoleSuccessful, lnkEditClientRolesSetFocusView);
                        AnyAuthzChanges = true;
                        if( null == mUser)
                        { 
                            mUser = new Data.User();
                            mUser.UID = hiddenUID;
                        }
                    }
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating user client role in kgs user administration.", sqlEx);
                    LabelHelper.SetLabelMessage(lblEditClientRolesError, updateRoleFailed, lnkEditClientRolesSetFocusView);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error updating user client role in kgs user administration.", ex);
                    LabelHelper.SetLabelMessage(lblEditClientRolesError, updateRoleFailed, lnkEditClientRolesSetFocusView);
                }
            }

            /// <summary>
            /// Update client modules Button on click.
            /// </summary>
            protected void updateClientModulesButton_Click(object sender, EventArgs e)
            {
                //get hidden fields
                int hiddenUID = Convert.ToInt32(hdnEditClientModulesUID.Value);
                int hiddenPrimaryRoleID = Convert.ToInt32(hdnEditClientModulesPrimaryRoleID.Value);
                int hiddenSecondaryRoleID = Convert.ToInt32(hdnEditClientModulesSecondaryRoleID.Value);

                int cid = Convert.ToInt32(ddlEditClientModulesClient.SelectedValue);

                try
                {
                    //only if checked
                    if (chkIsModuleRestricted.Checked)
                    {
                        UpdateAssociationsModules(hiddenUID, cid, lbEditClientModulesTop, lbEditClientModulesBottom, hdnEditClientModulesPrimaryRoleID, hdnEditClientModulesSecondaryRoleID, hdnEditClientModulesUnrestricted);
                    }

                    LabelHelper.SetLabelMessage(lblEditClientModulesError, updateModulesSuccess, lnkEditClientModulesSetFocusView);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning user client to multiple modules groups.", ex);
                    LabelHelper.SetLabelMessage(lblEditClientModulesError, updateModulesFailed, lnkEditClientModulesSetFocusView);
                }

                //if all modules restrictions existed and are removed, set the user back to full mode if buildings and building gorups are not restricted.
                if (!chkIsModuleRestricted.Enabled && lbEditClientModulesBottom.Items.Count == 0)
                {
                    SetUserBackToFullMode(hiddenUID, cid, hiddenPrimaryRoleID, hiddenSecondaryRoleID);
                }

                //if all modules restrictions existed and are removed, set the user back to full mode
                //if (!chkIsModuleRestricted.Enabled && lbEditClientModulesBottom.Items.Count == 0)
                //{
                //    var buildingIds = mDataManager.UserClientDataMapper.GetUsersClients(hiddenUID, cid).Where(c => c.BID != 0).Select(b => b.BID).Distinct().ToArray();
                //    var buildingGroupIds = mDataManager.UserClientDataMapper.GetUsersClients(hiddenUID, cid).Where(c => c.BuildingGroupID != 0).Select(bg => bg.BuildingGroupID).Distinct().ToArray();

                //    //if biulding or building group restricted
                //    if (buildingIds.Any() || buildingGroupIds.Any())
                //    {
                //        //if building restricted then add for each building
                //        if (buildingIds.Any())
                //        {
                //            //for each building 
                //            foreach (int bid in buildingIds)
                //            {
                //                mDataManager.UserClientDataMapper.AddUsersClients(new UsersClients() { CID = cid, UID = hiddenUID, Guid = new Guid(), BID = bid, BuildingGroupID = 0, ModuleID = 0, PrimaryRoleID = hiddenPrimaryRoleID, SecondaryRoleID = hiddenSecondaryRoleID, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow });
                //            }
                //        }

                //        //if building restricted then add for each building
                //        if (buildingIds.Any())
                //        {
                //            //for each buildinggroup 
                //            foreach (int buildingGroupId in buildingGroupIds)
                //            {
                //                mDataManager.UserClientDataMapper.AddUsersClients(new UsersClients() { CID = cid, UID = hiddenUID, Guid = new Guid(), BID = 0, BuildingGroupID = buildingGroupId, ModuleID = 0, PrimaryRoleID = hiddenPrimaryRoleID, SecondaryRoleID = hiddenSecondaryRoleID, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow });
                //            }
                //        }
                //    }
                //    else
                //    {
                //        mDataManager.UserClientDataMapper.AddUsersClients(new UsersClients() { CID = cid, UID = hiddenUID, Guid = new Guid(), BID = 0, BuildingGroupID = 0, ModuleID = 0, PrimaryRoleID = hiddenPrimaryRoleID, SecondaryRoleID = hiddenSecondaryRoleID, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow });
                //    }                    
                //}

                //rebind so just assigned modules have guids and such
                ddlEditClientModulesClient_OnSelectedIndexChanged(null, null);
            }

            protected void updateClientQuickLinksButton_Click(object sender, EventArgs e)
            {
                //get hidden uid
                int hiddenUID = Convert.ToInt32(hdnEditClientQuickLinksUID.Value);

                //get selected client
                int selectedCID = Convert.ToInt32(ddlEditClientQuickLinksClient.SelectedValue);

                //get primaryroleid
                string hiddenPrimaryRoldID = hdnEditClientQuickLinksPrimaryRoleID.Value;

                try
                {
                    //get quicklinks assigned for user and client
                    IEnumerable<Users_Clients_QuickLink> currentAssociatedUserClientQuickLinks = DataMgr.QuickLinksDataMapper.GetAllUsersClientsQuickLinks(hiddenUID, selectedCID);

                    //inserting each UserQuickLink in the bottom listbox if they dont exist
                    foreach (ListItem item in lbEditClientQuickLinksBottom.Items)
                    {
                        //declare new Users_Clients_QuickLink
                        Users_Clients_QuickLink mUserClientQuickLink = new Users_Clients_QuickLink();

                        int userClientQuickLinkID = Convert.ToInt32(item.Value);

                        if (!currentAssociatedUserClientQuickLinks.Any(u_c_ql => u_c_ql.QuickLinkID == userClientQuickLinkID))
                        {
                            mUserClientQuickLink.QuickLinkID = userClientQuickLinkID;
                            mUserClientQuickLink.UID = hiddenUID;
                            mUserClientQuickLink.CID = selectedCID;
                            DataMgr.QuickLinksDataMapper.InsertUserClientQuickLink(mUserClientQuickLink);
                        }
                    }

                    //deleting each quicklink in the top listbox if they exist
                    foreach (ListItem item in lbEditClientQuickLinksTop.Items)
                    {
                        //this is a quicklink id not a user_client_quicklink id
                        int quickLinkID = Convert.ToInt32(item.Value);

                        if (currentAssociatedUserClientQuickLinks.Any(u_c_ql => u_c_ql.QuickLinkID == quickLinkID))
                        {
                            //if the quicklink exists, delete
                            //if (QuickLinksDataMapper.DoesQuickLinkExistForUser(Convert.ToInt32(item.Value), mUser.UserID))
                            //{
                            DataMgr.QuickLinksDataMapper.DeleteUserClientQuickLinksByCIDAndUIDAndQuickLinkID(selectedCID, hiddenUID, quickLinkID);
                            //}
                        }
                    }

                    LabelHelper.SetLabelMessage(lblEditClientQuickLinksError, updateQuickLinksSuccessful, lnkEditClientQuickLinksSetFocusView);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning quicklinks to user in kgs user administration.", sqlEx);
                    LabelHelper.SetLabelMessage(lblEditClientQuickLinksError, updateQuickLinksFailed, lnkEditClientQuickLinksSetFocusView);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning quicklinks to user in kgs user administration.", ex);
                    LabelHelper.SetLabelMessage(lblEditClientQuickLinksError, updateQuickLinksFailed, lnkEditClientQuickLinksSetFocusView);
                }

                //rebind quicklinks so associated ids are with new items in the bottom listbox
                ddlEditClientQuickLinksClient_OnSelectedIndexChanged(null, null);
            }

            /// <summary>
            /// Update building Button on click.
            /// </summary>
            protected void updateClientBuildingsButton_Click(object sender, EventArgs e)
            {
                //get hidden fields
                int hiddenUID = Convert.ToInt32(hdnEditClientBuildingsUID.Value);
                int hiddenPrimaryRoleID = Convert.ToInt32(hdnEditClientBuildingsPrimaryRoleID.Value);
                int hiddenSecondaryRoleID = Convert.ToInt32(hdnEditClientBuildingsSecondaryRoleID.Value);

                int cid = Convert.ToInt32(ddlEditClientBuildingsClient.SelectedValue);

                try
                {
                    //only if checked
                    if (chkIsBuildingRestricted.Checked)
                    {
                        UpdateAssociationsBuildings(hiddenUID, cid, lbEditClientBuildingsTop, lbEditClientBuildingsBottom, hdnEditClientBuildingsPrimaryRoleID, hdnEditClientBuildingsSecondaryRoleID, hdnEditClientBuildingsUnrestricted);
                        UpdateAssociationsBuildingsGroups(hiddenUID, cid, lbEditClientBuildingGroupsTop, lbEditClientBuildingGroupsBottom, hdnEditClientBuildingsPrimaryRoleID, hdnEditClientBuildingsSecondaryRoleID, hdnEditClientBuildingsUnrestricted);
                        AnyAuthzChanges = true;
                    }

                    LabelHelper.SetLabelMessage(lblEditClientBuildingsError, updateBuildingsSuccess, lnkEditClientBuildingsSetFocusView);
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error reassigning user client to multiple buildings\building groups.", ex);
                    LabelHelper.SetLabelMessage(lblEditClientBuildingsError, updateBuildingsFailed, lnkEditClientBuildingsSetFocusView);
                }


                //if all building and group restrictions existed and are removed, set the user back to full mode if modules are not restricted.
                if (!chkIsBuildingRestricted.Enabled && lbEditClientBuildingsBottom.Items.Count == 0 && lbEditClientBuildingGroupsBottom.Items.Count == 0)
                {
                    SetUserBackToFullMode(hiddenUID, cid, hiddenPrimaryRoleID, hiddenSecondaryRoleID);
                    AnyAuthzChanges = true;
                }

                ////if all building and group restrictions existed and are removed, set the user back to full mode
                //if (!chkIsBuildingRestricted.Enabled && lbEditClientBuildingsBottom.Items.Count == 0 && lbEditClientBuildingGroupsBottom.Items.Count == 0)
                //{
                //    //var moduleIds = mDataManager.UserClientDataMapper.GetUsersClients(hiddenUID, cid).Where(c => c.ModuleID != 0).Select(m => m.ModuleID).Distinct().ToArray();

                //    if (moduleIds.Any())
                //    {
                //        //for each module 
                //        foreach (int moduleID in moduleIds)
                //        {
                //            mDataManager.UserClientDataMapper.AddUsersClients(new UsersClients() { CID = cid, UID = hiddenUID, Guid = new Guid(), BID = 0, BuildingGroupID = 0, ModuleID = moduleID, PrimaryRoleID = hiddenPrimaryRoleID, SecondaryRoleID = hiddenSecondaryRoleID, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow });
                //        }
                //    }
                //    else
                //    {
                //        mDataManager.UserClientDataMapper.AddUsersClients(new UsersClients() { CID = cid, UID = hiddenUID, Guid = new Guid(), BID = 0, BuildingGroupID = 0, ModuleID = 0, PrimaryRoleID = hiddenPrimaryRoleID, SecondaryRoleID = hiddenSecondaryRoleID, DateCreated = DateTime.UtcNow, DateModified = DateTime.UtcNow });
                //    }
                //}

                if(AnyAuthzChanges && null == mUser)
                {
                    mUser = new Data.User();
                    mUser.UID = hiddenUID;
                }
                //rebind so just assigned buidlings have guids and such
                ddlEditClientBuildingsClient_OnSelectedIndexChanged(null, null);
            }

            /// <summary>
            /// Update building modules Button on click.
            /// </summary>
            protected void updateClientBuildingModulesButton_Click(object sender, EventArgs e)
            {
                //TODO: next version

                //get hidden uid
                //int hiddenUID = Convert.ToInt32(hdnEditClientBuildingModulesUID.Value);
            }

            /// <summary>
            /// Search button on click, searches by all individual words entered and intersects.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void searchButton_Click(object sender, EventArgs e)
            {
                sessionState["Search"] = txtSearch.Text;
                BindUsers();

                pnlEditUsers.Visible = false;
            }

            /// <summary>
            /// Rebinds grid to view all.
            /// </summary>
            /// <param name="sender"></param>
            /// <param name="e"></param>
            protected void viewAll_Click(object sender, EventArgs e)
            {
                ResetForm();
            }

            protected void regenratePassword_Click(Object sender, EventArgs e) { SendTemporaryPassword(Convert.ToInt32(hdnEditUID.Value), lblResults, lblErrors, lnkSetFocusView); }

        #endregion

        #region Tab Events

            /// <summary>
            /// Tab changed event, bind users grid after tab changed back from add user tab
            /// </summary>
            protected void onTabClick(object sender, EventArgs e)
            {
                if (radTabStrip.MultiPage.SelectedIndex == 0)
                    ResetForm();
                else if (radTabStrip.MultiPage.SelectedIndex == 1)
                {
                    ddlAddOrganization.SelectedValue = "-1";
                    txtAddFirstName.Text = String.Empty;
                    txtAddLastName.Text = String.Empty;
                    txtAddEmail.Text = String.Empty;
                    txtAddConfirmEmail.Text = String.Empty;
                    txtAddAddress.Text = String.Empty;
                    txtAddCity.Text = String.Empty;
                    ddlAddCountry.SelectedValue = "-1";
                    ddlAddState.SelectedValue = "-1";
                    txtAddZip.Text = String.Empty;
                    txtAddPhone.Text = String.Empty;
                    txtAddMobilePhone.Text = String.Empty;
                    ddlAddRole.SelectedValue = "-1";
                    chkAddOrganizationAdmin.Checked = false;
                    chkAddTestUser.Checked = false;
                }
                else
                {
                    BindUsersAudit();
                }
            }

        #endregion

        #region Grid Events (Users)

            protected void gridUsers_OnRowCommand(object sender, GridViewCommandEventArgs e)
            {
                if (e.CommandSource is LinkButton)
                {
                    //get row
                    GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                    //if the on row command is executed by the paging links, then return
                    if (row.DataItemIndex < 0) return;

                    int index = row.DataItemIndex >= gridUsers.PageSize ? row.DataItemIndex % gridUsers.PageSize : row.DataItemIndex;

                    //get the datakey uid
                    int uid = Convert.ToInt32(gridUsers.DataKeys[index].Values["UID"]);

                    string hdnOID = ((HiddenField)gridUsers.Rows[index].Cells[5].Controls[3]).Value;

                    switch (e.CommandArgument.ToString().ToUpper())
                    {
                        case "EDITCLIENTROLES":

                            string hdnPrimaryRoleID = ((HiddenField)gridUsers.Rows[index].Cells[5].Controls[1]).Value;

                            SetUserClientRolesIntoEditForm(uid, hdnPrimaryRoleID, hdnOID);

                            //hide other panels
                            HidePanelsExcept(pnlEditClientRoles.ID);

                            //hide lblError if was set by editing previously
                            lblEditClientRolesError.Visible = false;
                            
                            break;
                        case "EDITCLIENTMODULES":
                            SetUserClientModulesIntoEditForm(uid);

                            //hide other panels
                            HidePanelsExcept(pnlEditClientModules.ID);

                            //hide lblError if was set by editing previously
                            lblEditClientModulesError.Visible = false;

                            break;
                        case "EDITCLIENTQUICKLINKS":

                            string hdnPrimaryRoleID2 = ((HiddenField)gridUsers.Rows[index].Cells[5].Controls[1]).Value;

                            SetUserClientQuickLinksIntoEditForm(uid, hdnPrimaryRoleID2);

                            //hide other panels
                            HidePanelsExcept(pnlEditClientQuickLinks.ID);

                            //hide lblError if was set by editing previously
                            lblEditClientQuickLinksError.Visible = false;

                            break;
                        case "EDITBUILDINGS":
                            SetUserClientBuildingsIntoEditForm(uid, hdnOID);                            

                            //hide other panels
                            HidePanelsExcept(pnlEditClientBuildings.ID);

                            //hide lblError if was set by editing previously
                            lblEditClientBuildingsError.Visible = false;

                            break;
                        case "EDITBUIDLINGMODULES":
                            SetUserClientBuildingModulesIntoEditForm(uid);

                            //hide other panels
                            HidePanelsExcept(pnlEditClientBuildingModules.ID);

                            //hide lblError if was set by editing previously
                            lblEditClientBuildingModulesError.Visible = false;

                            break;
                        case "UNLOCKUSER":
                            try
                            {
                                //unlock and clear failed attmpts
                                UsersAudit ua = DataMgr.UserAuditDataMapper.GetUserAuditByUID(uid);                               
                                ua.LoginFailedAttemptCount = 0;
                                DataMgr.UserAuditDataMapper.UpdateUserAudit(ua);

                                mUser = new User();
                                mUser.UID = uid;
                                mUser.IsLocked = false;
                                DataMgr.UserDataMapper.UpdateUserLock(mUser);

                                lblErrors.Text = unlockSuccessful;
                                lblErrors.Visible = true;
                                lnkSetFocusView.Focus();

                                pnlEditUsers.Visible = false;
                                dtvUser.Visible = false;
                            }
                            catch (Exception ex)
                            {
                                lblErrors.Text = unlockFailed;
                                lblErrors.Visible = true;
                                lnkSetFocusView.Focus();

                                LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error unlocking user in kgs user administration: UserId={0}, UserEmail={1}, UserFirstName={2} UserLastName={3}.", mUser.UID, mUser.Email, mUser.FirstName, mUser.LastName), ex);
                            }
                            
                            RebindUsers();

                            break;
                    }
                }
            }

            protected void gridUsers_OnDataBound(object sender, EventArgs e)
            {
                GridViewRowCollection rows = gridUsers.Rows;

                //if not kgs dev role then...
                //check if hidden role id is less than or equal to current users role id. 
                //if so then disable the edit button. 
                //users with same role id can only view others, not edit.
                //you cannot edit youself because you cant change your role, or deactivate yourself.
                //goto myaccount for self edit.

                foreach (GridViewRow row in rows)
                {
                    if ((row.RowState & DataControlRowState.Edit) == 0)
                    {
                        string hdnPrimaryRoleID = ((HiddenField)row.Cells[5].Controls[1]).Value;

                        try
                        {
                            if (!siteUser.IsKGSSystemAdmin)
                            {
                                if (Convert.ToInt32(hdnPrimaryRoleID) <= siteUser.RoleID)
                                {
                                    //hide all edit links
                                    ((LinkButton)row.Cells[1].Controls[1]).Visible = false;
                                    ((LiteralControl)row.Cells[1].Controls[2]).Visible = false;
                                    ((LinkButton)row.Cells[1].Controls[3]).Visible = false;
                                    ((LiteralControl)row.Cells[1].Controls[4]).Visible = false;
                                    ((LinkButton)row.Cells[1].Controls[5]).Visible = false;
                                    ((LiteralControl)row.Cells[1].Controls[6]).Visible = false;
                                    ((LinkButton)row.Cells[1].Controls[7]).Visible = false;
                                    ((LiteralControl)row.Cells[1].Controls[8]).Visible = false;
                                    ((LinkButton)row.Cells[1].Controls[9]).Visible = false;
                                    ((LiteralControl)row.Cells[1].Controls[10]).Visible = false;
                                    ((LinkButton)row.Cells[1].Controls[11]).Visible = false;
                                    ((LiteralControl)row.Cells[1].Controls[12]).Visible = false;
                                    //delete and lock link
                                    ((LinkButton)row.Cells[8].Controls[1]).Visible = false;
                                    ((LinkButton)row.Cells[9].Controls[1]).Visible = false;
                                }
                            }

                            if (!RoleHelper.IsRestricted(hdnPrimaryRoleID))
                            {
                                //hide all except edit user and quicklinks
                                ((LinkButton)row.Cells[1].Controls[3]).Visible = false;
                                ((LiteralControl)row.Cells[1].Controls[4]).Visible = false;
                                ((LinkButton)row.Cells[1].Controls[5]).Visible = false;
                                ((LiteralControl)row.Cells[1].Controls[6]).Visible = false;
                            
                                ((LiteralControl)row.Cells[1].Controls[8]).Visible = false;
                                ((LinkButton)row.Cells[1].Controls[9]).Visible = false;
                                ((LiteralControl)row.Cells[1].Controls[10]).Visible = false;
                                ((LinkButton)row.Cells[1].Controls[11]).Visible = false;
                                ((LiteralControl)row.Cells[1].Controls[12]).Visible = false;
                            }

                            if (RoleHelper.IsPublicOrKioskUser(hdnPrimaryRoleID))
                            {
                                //hide role change for public and kiosk users
                                ((LinkButton)row.Cells[1].Controls[3]).Visible = false;
                                ((LiteralControl)row.Cells[1].Controls[4]).Visible = false;
                            }
                        }
                        catch
                        {
                        }
                    }
                }
            }
                  
            protected void gridUsers_OnSelectedIndexChanged(object sender, EventArgs e)
            {
                int uid = Convert.ToInt32(gridUsers.DataKeys[gridUsers.SelectedIndex].Values["UID"]);

                //Get entered user
                //mUser = mDataManager.UserDataMapper.GetUser(uid);

                //get primary role
                string primaryRoleID = ((HiddenField)gridUsers.Rows[gridUsers.SelectedIndex].Cells[5].Controls[1]).Value.ToString();

                //set data source
                dtvUser.DataSource = DataMgr.UserDataMapper.GetFullUser(uid, Convert.ToInt32(primaryRoleID), RoleHelper.IsKGSFullAdminOrHigher(primaryRoleID), RoleHelper.IsRestricted(primaryRoleID)); // mDataManager.UserDataMapper.GetUser(userID);
                //bind user to details view
                dtvUser.DataBind();
                
                //reset border on all rows
                //for (int i = 0; i < gridUsers.Rows.Count; i++)
                //{
                //    gridUsers.Rows[i].BorderWidth = 0;
                //}

                //set seleted rows border as darkred
                //gridUsers.SelectedRow.BorderColor = System.Drawing.Color.DarkRed;
                //gridUsers.SelectedRow.BorderWidth = 1;                  

                HidePanelsExcept(dtvUser.ID);
            }

            protected void gridUsers_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridUsers_Editing(object sender, GridViewEditEventArgs e)
            {
                //gridUsers.EditIndex = e.NewEditIndex;

                ////get selected dropdown value to set selected value after bound again
                //try
                //{
                //    //selectedValue = gridUsers.Rows[e.NewEditIndex].Cells[5].Text;
                //    //selectedValue = ((DataBoundLiteralControl)gridUsers.Rows[e.NewEditIndex].Cells[4].Controls[0]).Text;
                //    selectedValue = ((HiddenField)gridUsers.Rows[e.NewEditIndex].Cells[5].Controls[1]).Value;
                //}
                //catch
                //{
                //}

                ////Bind data again keeping current page and sort mode
                //DataTable dataTable = ConvertIEnumerableToDataTable(QueryUsers());
                //gridUsers.PageIndex = gridUsers.PageIndex;
                //gridUsers.DataSource = SortDataTable(dataTable as DataTable, true);
                //gridUsers.DataBind();     

                
                HidePanelsExcept(pnlEditUsers.ID);

                //hide lblError if was set by editing previously
                lblEditError.Visible = false;

                int uid = Convert.ToInt32(gridUsers.DataKeys[e.NewEditIndex].Values["UID"]);

                mUser = DataMgr.UserDataMapper.GetUser(uid, true);

                string hdnPrimaryRoleID = ((HiddenField)gridUsers.Rows[e.NewEditIndex].Cells[5].Controls[1]).Value;

                SetUserIntoEditForm(mUser, hdnPrimaryRoleID);

                //reset border on all rows
                //for (int i = 0; i < gridUsers.Rows.Count; i++)
                //{
                //    gridUsers.Rows[i].BorderWidth = 0;
                //}

                //set edit rows border as darkred
                //gridUsers.Rows[e.NewEditIndex].BorderColor = System.Drawing.Color.DarkRed;
                //gridUsers.Rows[e.NewEditIndex].BorderWidth = 1;

                //Cancels the edit auto grid selected. Fixes the select link diasspearing in selected mode, and the edit link performing the delete method.
                e.Cancel = true;
            }

            protected void gridUsers_Deleting(object sender, GridViewDeleteEventArgs e)
            {                
                //get deleting rows uid
                int uid = Convert.ToInt32(gridUsers.DataKeys[e.RowIndex].Value);

                try
                {
                    //delete user and all associations
                    DataMgr.UserDataMapper.DeleteUserAndAllAssociations(uid);

                    lblErrors.Text = deleteSuccessful;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    pnlEditUsers.Visible = false;
                    dtvUser.Visible = false;
                }
                catch (Exception ex)
                {
                    lblErrors.Text = deleteFailed;
                    lblErrors.Visible = true;
                    lnkSetFocusView.Focus();

                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("Error deleting user in kgs user administration: UserId={0}, UserEmail={1}, UserFirstName={2} UserLastName={3}.", mUser.UID, mUser.Email, mUser.FirstName, mUser.LastName), ex);
                }

                RebindUsers();
            }

            protected void gridUsers_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedUsers(sessionState["Search"].Split(' ')));

                //maintain current sort direction and expresstion on paging
                gridUsers.DataSource = SortDataTable(dataTable, true);
                gridUsers.PageIndex = e.NewPageIndex;
                gridUsers.DataBind();
            }

            protected void gridUsers_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                gridUsers.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QuerySearchedUsers(sessionState["Search"].Split(' ')));

                GridViewSortExpression = e.SortExpression;

                gridUsers.DataSource = SortDataTable(dataTable, false);
                gridUsers.DataBind();
            }
        
        #endregion         

        #region Grid Events (Users Audit)

            protected void gridUsersAudit_OnDataBound(object sender, EventArgs e)
            {
            }

            protected void gridUsersAudit_OnRowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.Pager)
                {
                    //Note: The autogenerated pager table is not shown with just one page.

                    Table tblPager = (Table)e.Row.Cells[0].Controls[0];
                    tblPager.CssClass = "pageTable";

                    TableRow theRow = tblPager.Rows[0];

                    LinkButton ctrlPrevious = new LinkButton();
                    ctrlPrevious.CommandArgument = "Prev";
                    ctrlPrevious.CommandName = "Page";
                    ctrlPrevious.Text = "« Previous Page";
                    ctrlPrevious.CssClass = "pageLink";

                    TableCell cellPreviousPage = new TableCell();
                    cellPreviousPage.Controls.Add(ctrlPrevious);
                    tblPager.Rows[0].Cells.AddAt(0, cellPreviousPage);


                    LinkButton ctrlNext = new LinkButton();
                    ctrlNext.CommandArgument = "Next";
                    ctrlNext.CommandName = "Page";
                    ctrlNext.Text = "Next Page »";
                    ctrlNext.CssClass = "pageLink";


                    TableCell cellNextPage = new TableCell();
                    cellNextPage.Controls.Add(ctrlNext);
                    tblPager.Rows[0].Cells.Add(cellNextPage);
                }
            }

            protected void gridUsersAudit_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryUsersAudit());

                //maintain current sort direction and expresstion on paging
                gridUsersAudit.DataSource = UsersAuditSortDataTable(dataTable, true);
                gridUsersAudit.PageIndex = e.NewPageIndex;
                gridUsersAudit.DataBind();
            }

            protected void gridUsersAudit_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Reset the edit index.
                //gridUsersAudit.EditIndex = -1;

                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryUsersAudit());

                GridViewUsersAuditSortExpression = e.SortExpression;

                gridUsersAudit.DataSource = UsersAuditSortDataTable(dataTable, false);
                gridUsersAudit.DataBind();
            }

        #endregion

        #region Bind QuickLinks

            /// <summary>
            /// Binds the quick links by user and client.
            /// </summary>
            private void BindUserClientQuickLinks(int uid, int cid, int roleID)
            {
                //clear quicklink listboxes
                ClearListbox(lbEditClientQuickLinksTop);
                ClearListbox(lbEditClientQuickLinksBottom);

                //get based on user client modules restriction
                //get available quicklinks not assigned by uid, cid, and role
                IEnumerable<QuickLink> availableQuickLinks = DataMgr.QuickLinksDataMapper.GetAllQuickLinksAvailableForUser(uid, cid, roleID, false);

                //bind available user quicklinks
                BindAvailableUserQuickLinksLoop(availableQuickLinks);

                //get and bind assigned user client quicklins.
                BindAssignedUserQuickLinksLoop(DataMgr.QuickLinksDataMapper.GetAllQuickLinksAssignedToUser(uid, cid));
            }

            /// <summary>
            /// for each available user quicklink, format and bind
            /// </summary>
            /// <param name="availableUserQuickLinks"></param>
            private void BindAvailableUserQuickLinksLoop(IEnumerable<QuickLink> availableUserQuickLinks)
            {
                //for each available user QuickLink              
                using (IEnumerator<QuickLink> list = availableUserQuickLinks.GetEnumerator())
                {
                    int counter = 1;
                    while (list.MoveNext())
                    {
                        //current user QuickLink
                        QuickLink item = (QuickLink)list.Current;

                        ListItem listItem = new ListItem(item.QuickLinkName, item.QuickLinkID.ToString());

                        lbEditClientQuickLinksTop.Items.Add(listItem);
                        
                        counter++;
                    }
                }
            }

            /// <summary>
            /// this is only if in user edit mode.
            /// for each assigned user QuickLink, format and bind
            /// </summary>
            /// <param name="assignedUserQuickLinks"></param>
            private void BindAssignedUserQuickLinksLoop(IEnumerable<QuickLink> assignedUserQuickLinks)
            {
                //for each available user QuickLink              
                using (IEnumerator<QuickLink> list = assignedUserQuickLinks.GetEnumerator())
                {
                    int counter = 1;
                    while (list.MoveNext())
                    {
                        //current user QuickLink
                        QuickLink item = (QuickLink)list.Current;

                        ListItem listItem = new ListItem(item.QuickLinkName, item.QuickLinkID.ToString());

                        lbEditClientQuickLinksBottom.Items.Add(listItem);

                        counter++;
                    }
                }
            }
        
        #endregion

        #region Helper Methods (Users)

            private IEnumerable<GetUsersData> QueryUsers()
            {
                try
                {
                    //get all users by role and optionally clientid
                    return DataMgr.UserDataMapper.GetAllPartialUsersByOIDWithPrimaryRole(Convert.ToInt32(ddlOrganization.SelectedValue), true, true); //UserDataMapper.GetAllUsersByRoleIDAndOptionallyClientID(Convert.ToInt32(ddlRole.SelectedValue), divEditClientsListBoxs.Visible ? (int?)Convert.ToInt32(ddlClient.SelectedValue) : null);                    
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving users in kgs user administration.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving users in kgs user administration.", ex);
                    return null;
                }
            }

            private IEnumerable<GetUsersData> QuerySearchedUsers(string[] searchText)
            {
                try
                {
                    //get all users by role and optionally clientID
                    return DataMgr.UserDataMapper.GetAllPartialSearchedUsersByOIDWithPrimaryRole(searchText, Convert.ToInt32(ddlOrganization.SelectedValue), chkAllOrgs.Checked, true, true); //UserDataMapper.GetAllSearchedUsersByRoleIDAndOptionallyClientID(searchText, Convert.ToInt32(ddlRole.SelectedValue), divClient.Visible ? (int?)Convert.ToInt32(ddlClient.SelectedValue) : null);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving users in kgs user administration.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving users in kgs user administration.", ex);
                    return null;
                }
            }

            private void BindUsers()
            {
                //query users
                IEnumerable<GetUsersData> users = String.IsNullOrWhiteSpace(txtSearch.Text) ? QueryUsers() : QuerySearchedUsers(txtSearch.Text.Split(' '));

                //maintain sort--- doesnt work!
                //SortDirection tempSD = GridViewSortDirection == "ASC" ? SortDirection.Ascending : SortDirection.Descending;
                //gridUsers.Sort(GridViewSortExpression, tempSD);

                gridUsers.DataSource = users;

                // bind grid
                gridUsers.DataBind();

                var count = users.Count();

                SetGridCountLabel(count, lblResults);

                //set search visibility. dont remove when last result from searched results in the grid is deleted.
                pnlSearch.Visible = (String.IsNullOrWhiteSpace(txtSearch.Text) && count == 0) ? false : true;
            }

            private void RebindUsers()
            {
                DataTable dataTable = EnumerableHelper.ConvertIEnumerableToDataTable(QueryUsers());
                gridUsers.PageIndex = gridUsers.PageIndex;
                gridUsers.DataSource = SortDataTable(dataTable as DataTable, true);
                gridUsers.DataBind();

                SetGridCountLabel(dataTable.Rows.Count, lblResults);

                //hide edit form if shown
                pnlEditUsers.Visible = false;
            }

        #endregion

        #region Helper Methods (Users Audit)

            private IEnumerable<GetUsersAuditData> QueryUsersAudit()
            {
                try
                {
                    //get all users audits
                    return DataMgr.UserAuditDataMapper.GetAllUsersAuditsByOID(globalOID, true);
                }
                catch (SqlException sqlEx)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving users audits.", sqlEx);
                    return null;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, "Error retrieving users audits.", ex);
                    return null;
                }
            }

            private void BindUsersAudit()
            {
                //query users audits
                IEnumerable<GetUsersAuditData> usersAudits = QueryUsersAudit();

                int count = usersAudits.Count();

                gridUsersAudit.DataSource = usersAudits;

                // bind grid
                gridUsersAudit.DataBind();

                SetGridUsersAuditCountLabel(count, lblUsersAuditResults);
            }

        #endregion

        #region Helper Methods

            public string GenerateClientList(List<GetClientsWithRoleFormat> clients)
            {
                int count = clients.Count;
                int counter = 1;
                string returnString = "";

                foreach (GetClientsWithRoleFormat c in clients)
                {
                    returnString += c.ClientName + " - " + c.RoleName;
                    returnString += counter != count ? ", " : "";
                    counter++;
                }

                return returnString;
            }

            private void RearrangeAddForm(string selectedOrganizationValue, string selectedRoleValue)
            {
                //based on role enable or disable and clear orgadmin checkbox. only suuper admins or kgs system admins can be an orgadmin
                if (RoleHelper.IsKGSSuperAdminOrHigher(selectedRoleValue) || RoleHelper.IsSuperAdmin(selectedRoleValue))
                {
                    chkAddOrganizationAdmin.Enabled = true;
                }
                else
                {
                    chkAddOrganizationAdmin.Checked = false;
                    chkAddOrganizationAdmin.Enabled = false;
                }

                //clear listboxes
                lbAddClientsTop.Items.Clear();
                lbAddClientsBottom.Items.Clear();


                //if any role with restriction, show client multi select boxes 
                if (RoleHelper.IsRestricted(selectedRoleValue))
                {
                    divAddClientsListBoxs.Visible = true;

                    //bind clients to listbox on add user page
                    //get all clients if kgs client restricted, else get all clients asssocaited to org including provider clients 
                    IEnumerable<Client> clients = RoleHelper.IsKGSRestrictedAdmin(selectedRoleValue) ? DataMgr.ClientDataMapper.GetAllClients() : DataMgr.ClientDataMapper.GetAllActiveClientsByOrgWithProviderClients(Convert.ToInt32(selectedOrganizationValue));

                    //bind clients to top listbox
                    lbAddClientsTop.DataSource = clients;
                    lbAddClientsTop.DataTextField = "ClientName";
                    lbAddClientsTop.DataValueField = "CID";
                    lbAddClientsTop.DataBind();
                }
                else
                {
                    divAddClientsListBoxs.Visible = false;
                }
            }

            private void HidePanelsExcept(string id)
            {
                //hide all pannels exept the one passed in

                pnlEditUsers.Visible = id == pnlEditUsers.ID ? true : false;
                pnlEditClientRoles.Visible = id == pnlEditClientRoles.ID ? true : false;
                pnlEditClientModules.Visible = id == pnlEditClientModules.ID ? true : false;
                pnlEditClientQuickLinks.Visible = id == pnlEditClientQuickLinks.ID ? true : false;
                //pnlEditClientBuildingGroups.Visible = id == pnlEditClientBuildingGroups.ID ? true : false;
                pnlEditClientBuildings.Visible = id == pnlEditClientBuildings.ID ? true : false;
                pnlEditClientBuildingModules.Visible = id == pnlEditClientBuildingModules.ID ? true : false;

                dtvUser.Visible = id == dtvUser.ID ? true : false; ;
            }

            private void ResetForm()
            {
                txtSearch.Text = String.Empty;
                sessionState["Search"] = String.Empty;
                BindUsers();

                pnlEditUsers.Visible = false;
            }

            private bool EmailUser()
            {                           
                //try to email the user
                try
                {
                    //hash a new random password
                    string randomPassword = Membership.GeneratePassword(12, 2);
                    string newHashedPassword = Encryptor.getMd5Hash(randomPassword);
                    mUser.Password = newHashedPassword;

                    //get support override if exists
                    string supportOverrideEmail = DataMgr.ClientDataMapper.GetSupportOverrideEmailIfExists(siteUser.CID);

                    bool? hide = GetShowPwdState(mUser.OID);
                    if (!hide.Value)
                    {
                        Email.SendNewUserAccountEmail(mUser, siteUser.IsSchneiderTheme, randomPassword, supportOverrideEmail);
                    }
                    else
                    {
                        // For externally managed passwords, generate a different email with instructions
                        Email.SendNewUserAccountEmail(mUser, siteUser.IsSchneiderTheme);
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    LogMgr.Log(DataConstants.LogLevel.ERROR, String.Format("New user account email was not sent for user: UserEmail={0}.", mUser.Email), ex);
                    LabelHelper.SetLabelMessage(lblAddError, addUserFailed, lnkSetFocusAdd);
                    return false;
                }
            }

            private void ShowPwdArea(int oid)
            {
                GetSSOInfoAsync(oid).Wait();
            }

            /// <summary>
            /// Returns true if externally managed IdP; otherwise false
            /// </summary>
            /// <param name="organizationid"></param>
            /// <returns></returns>
            private bool? GetShowPwdState(int organizationid)
            {
                string oid = organizationid.ToString();
                bool? hide = null;
                string map = hdnOIDToAllowPwd.Value ?? "";
                if (!string.IsNullOrEmpty(map))
                {
                    string[] pairs = map.Split(',');
                    if (null != pairs)
                    {
                        for (int i = 0, iNum = pairs.Length; i < iNum && null == hide; i++)
                        {
                            string[] oidToVal = pairs[i].Split(':');
                            if (null != oidToVal && oidToVal[0] == oid)
                            {
                                string v = oidToVal[1];
                                hide = ("1" == v);
                            }
                        }
                    }
                }
                return (hide);
            }

            private async Task GetSSOInfoAsync(int organizationid)
            {
                string oid = organizationid.ToString();
                bool? hide = GetShowPwdState(organizationid);
                if (null == hide)
                {
                    AuthNSSOInfo results = await AuthnHelper.GetSSOInfoAsync(organizationid).ConfigureAwait(false);
                    hide = results.SSOEnabled;

                    string map = hdnOIDToAllowPwd.Value ?? "";
                    if (map.Length > 0)
                    {
                        map = map + ",";
                    }
                    map = map + oid + ":" + (hide.Value ? "1" : "0");
                    hdnOIDToAllowPwd.Value = map;
                }

                if (hide.Value)
                {
                    externallyManaged.Style.Value = "display: block";
                    internallyManaged.Style.Value = "display: none";
                    newUserPwdMessage.Style.Value = "display:none";
                }
                else
                {
                    externallyManaged.Style.Value = "display: none";
                    internallyManaged.Style.Value = "display: block";
                    newUserPwdMessage.Style.Value = "display:block";
                }
                editNewPasswordRegExValidator.Enabled = !hide.Value;
                editNewPaswordRegExValidatorExtender.Enabled = !hide.Value;
                editConfirmNewPasswordRegExValidator.Enabled = !hide.Value;
                editConfirmNewPasswordRegExValidatorExtender.Enabled = !hide.Value;
                editConfirmNewPasswordRequiredValidator.Enabled = !hide.Value;
                editConfirmNewPasswordRequiredValidatorExtender.Enabled = !hide.Value;
                editConfirmNewPasswordCompareValidator.Enabled = !hide.Value;
                editConfirmNewPasswordCompareValidatorExtender.Enabled = !hide.Value;
                btnRegenratePassword.Enabled = !hide.Value;
            }
        #endregion
    }
}