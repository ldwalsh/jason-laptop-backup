﻿using CW.Common;
using CW.Common.Constants;
using CW.Logging;
using CW.Website._framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CW.Website.Logging
{
    public sealed class SessionLogger : ILogManager
    {
        #region fields

            readonly ILogManager mLogMgr;
            readonly IEnumerable<DataConstants.LogLevel> mLogLevels;

        #endregion

        #region constructor(s)

            public SessionLogger(ILogManager logMgr, IEnumerable<DataConstants.LogLevel> logLevels)
            {
                mLogMgr = logMgr;
                mLogLevels = logLevels;                                               
            }

        #endregion

        #region ILogManager implementation

            public void Log(DataConstants.LogSection loggerSection, DataConstants.LogLevel logLevel, string message, Exception ex = null, IUserIdentity user = null, string loggerName = "")
            {
                if (mLogLevels.Contains(logLevel))
                {
                    var ipAddressTxt = "IP Address";
                    var absoluteUriTxt = "Absolute Uri";

                    if (HttpContext.Current?.Request != null)
                    {
                        var guid = Guid.NewGuid().ToString();

                        if (HttpContext.Current.Session != null)
                            HttpContext.Current.Session["LastErrorOperationId"] = guid;

                        message = $"Operation Id: {guid} {message}, {ipAddressTxt}: {GetUserIP()}, {absoluteUriTxt}: {HttpContext.Current.Request.Url.AbsoluteUri}, User Id: {SiteUser.Current.UID}";
                    }
                    else
                        message = $"{message}";
                }

                if (SiteUser.Current != null)
                {
                    mLogMgr.Log(loggerSection, logLevel, message, ex, new Data.Models.Security.UserIdentity() { CID = SiteUser.Current.CID, UID = SiteUser.Current.UID });
                }
                else
                {
                    mLogMgr.Log(loggerSection, logLevel, message, ex);
                }
            }                                                   

        #endregion

        #region methods

            string GetUserIP() //Please refer to stack overflow for the below code http://stackoverflow.com/questions/735350/how-to-get-a-users-client-ip-address-in-asp-net
            {
                var ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipList)) return ipList.Split(',')[0];

                return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }

        #endregion
    }
}